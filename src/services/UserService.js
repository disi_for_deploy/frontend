import axios from "axios";
import {HOST} from './Hosts';
const AuthStrClient = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class UserService{

    getUserByUsername(username){
        return axios.get(HOST.api + '/users/name/' + username,
            { headers: { Authorization: AuthStrClient } });
    }

} export default new UserService()