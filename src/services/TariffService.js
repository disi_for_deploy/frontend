import axios from "axios";
import {HOST} from "./Hosts";
const AuthStrClient = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class TariffService{

    addTariff(tariff){
        return axios.post(HOST.api + "/tariffs", tariff,
            { headers: { Authorization: AuthStrClient }});
    }

    getTariffsByLocationId(idLocation){
        return axios.get(HOST.api + "/tariffs/location/" + idLocation)
    }

}export default new TariffService()