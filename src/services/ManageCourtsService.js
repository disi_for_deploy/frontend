import axios from "axios";
import {HOST} from './Hosts';

const AuthStr = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class ManageCourtService{

    addNewCourt(court){
        console.log("courttt =>", court);
        return axios.post(HOST.api + "/courts", court,
        { headers: { Authorization: AuthStr } });
    }

    deleteCourtById(idCourt){
        console.log("kjbdvkwbndv",idCourt)
        return axios.delete(HOST.api + "/courts/" + idCourt,
        { headers: { Authorization: AuthStr } });
    }

    getAllCourts(){
        return axios.get(HOST.api + "/courts");
    }

    updateCourtById(idCourt,court){
        return axios.put(HOST.api + "/courts/" + idCourt, court,
        { headers: { Authorization: AuthStr } });
    }

    getCourtById(idCourt){
        return axios.get(HOST.api + "/courts/" + idCourt);
    }
}

export default new ManageCourtService()