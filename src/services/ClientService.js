import axios from "axios";
import {HOST} from './Hosts';
const AuthStrClient = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class ClientService{

    getClient(username){
        return axios.get(HOST.api + '/clients/username/' + username,
            { headers: { Authorization: AuthStrClient } });
    }

    updateClient(client, client_id){
        return axios.put(HOST.api + '/clients/' + client_id, client,
                { headers: { Authorization: AuthStrClient } });
    }

    returnMoney(data){
        return axios.post(HOST.api + '/clients/returnMoney', data,
                { headers: { Authorization: AuthStrClient } });
    }

} export default new ClientService()