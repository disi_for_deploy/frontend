import axios from "axios";
import {HOST} from './Hosts';

const AuthStr = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class SubscriptionService{

    getSubscriptions(){
        return axios.get(HOST.api + "/subscriptions",
            { headers: { Authorization: AuthStr } });
    }

    getAvailableIntervals(data){
        return axios.post(HOST.api + "/subscriptions/availableIntervals", data,
            { headers: { Authorization: AuthStr } })
    }

    computePrice(subscription){
        return axios.post(HOST.api + "/subscriptions/computePrice", subscription,
            { headers: { Authorization: AuthStr } })
    }

    postSubscription(subscription){
        return axios.post(HOST.api + "/subscriptions", subscription,
            { headers: { Authorization: AuthStr } })
    }

}
export default new SubscriptionService()