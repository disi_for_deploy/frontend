import axios from "axios";
import {HOST} from './Hosts';

const AuthStr = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class ManageLocationsService{

    addNewLocation(locatie){
        return axios.post(HOST.api + "/locations", locatie,
        { headers: { Authorization: AuthStr } });
    }

    getLocationById(idLocatie){
        return axios.get(HOST.api + "/locations/" + idLocatie);
    }

    updateLocationById(idLocatie,locatie){
        return axios.put(HOST.api + "/locations/" + idLocatie, locatie,
        { headers: { Authorization: AuthStr } });
    }

    deleteLocationById(idLocatie){
        return axios.delete(HOST.api + "/locations/" + idLocatie,
        { headers: { Authorization: AuthStr } });
    }

    getAllLocations(){
        return axios.get(HOST.api + "/locations");
    }
}

export default new ManageLocationsService()