import axios from "axios";
import { HOST } from './Hosts';

const AuthStr = 'TennisApp_'.concat(localStorage.getItem('login_token'));

class ReservationService {

    getReservations() {
        return axios.get(HOST.api + "/reservations",
            { headers: { Authorization: AuthStr } });
    }

    cancelReservation(id){
        return axios.delete(HOST.api + "/reservations/" + id,
            { headers: { Authorization: AuthStr } });
    }

    getReservationById(id) {
        return axios.get(HOST.api + "/reservations/" + id,
            { headers: { Authorization: AuthStr } });
    }

    getAvailableIntervals(data) {
        return axios.post(HOST.api + "/reservations/availableIntervals", data)
    }

    computePrice(reservation) {
        return axios.post(HOST.api + "/reservations/computePrice", reservation,
            { headers: { Authorization: AuthStr } })
    }

    postReservation(reservation) {
        return axios.post(HOST.api + "/reservations", reservation,
            { headers: { Authorization: AuthStr } })
    }

    transferReservation(reservation){
        return axios.post(HOST.api + "/reservations/transfer", reservation,
            { headers: { Authorization: AuthStr } })
    }


    changeClient(data){
        return axios.post(HOST.api + "/reservations/changeClient", data,
            { headers: { Authorization: AuthStr } })
    }

    searchPartner(reservation) {
        return axios.post(HOST.api + "/reservations/search", reservation,
            { headers: { Authorization: AuthStr } })
    }

}

export default new ReservationService()