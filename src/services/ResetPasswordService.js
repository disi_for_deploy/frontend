import axios from "axios";
import {HOST} from './Hosts';

class ResetPasswordService{

    sendEmailForCode(email){
        return axios.post(HOST.api + "/reset/" + email);
    }

    sendResetPassword(password, email){
        return axios.post(HOST.api + "/reset/confirmation/" + email, password);
    }
}

export default new ResetPasswordService()