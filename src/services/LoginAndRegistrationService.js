import axios from "axios";
import {HOST} from './Hosts';
import swal from 'sweetalert'

class LoginAndRegistrationService{

    login(user){
        var qs = require('qs');
        var data = qs.stringify({
            'username': user.username,
            'password': user.password
        });

        return axios.post(HOST.api + "/login", data)
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.status);
                    swal("Warning", "Incorrect username or password", "warning");
                }
            });
    }

    registerUser(user_client){
        return axios.post(HOST.api + "/register", user_client);
    }

}

export default new LoginAndRegistrationService()