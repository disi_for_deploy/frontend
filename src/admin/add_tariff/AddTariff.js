import React, {Component} from "react";
import {withRouter} from "react-router";
import Select from 'react-select';
import './AddTariff.css';
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"
import TariffService from "../../services/TariffService";
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent";
import {Button, DialogActions} from "@material-ui/core";

const seasons = [
    { value: 'summer', label: 'Summer' },
    { value: 'winter', label: 'Winter' },
];

const nightOptions = [
    {value: 'true', label: 'Yes'},
    {value: 'false', label: 'No'},
]

const weekOptions = [
    {value:'workweek', label:'Workweek'},
    {value:'weekend', label:'Weekend'},
]

class AddTariff extends Component{

    constructor(props) {
        super(props);

        this.validator = new RegisterLoginValidator([{
            field: 'price',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter price.'
        }, {
            field: 'price',
            method: this.isNumber,
            validWhen: true,
            message: '* Price is not a valid number.'
        }, {
            field: 'season',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select season.'
        }, {
            field: 'night',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select night.'
        }, {
            field: 'dayOfWeek',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select day of week.'
        }]);

        this.state = {
            season: '',
            night: '',
            dayOfWeek: '',
            price: '',
            location: this.props.match.params.id,
            openDialog: false,
            validation: this.validator.valid(),
        }

        this.submitted = false;
     }

    isNumber(myString) { return /^[0-9]+$/.test(myString)}

    handleChangeSeason = (season) => {
        this.setState({ season }, () =>
            console.log(`Option selected:`, this.state.season.value)
        );
    };

    handleChangeNight = (night) => {
        this.setState({ night }, () =>
            console.log(`Option selected:`, this.state.night.value)
        );
    };

    handleChangeWeek = (dayOfWeek) => {
        this.setState({ dayOfWeek }, () =>
            console.log(`Option selected:`, this.state.dayOfWeek.value)
        );
    };

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    addTariff(){
        let tariff = {
            season: this.state.season.value,
            dayOfWeek: this.state.dayOfWeek.value,
            night: this.state.night.value,
            price: this.state.price,
            locationAddress: this.state.location
        }

        console.log('tariff => ' + JSON.stringify(tariff));
        TariffService.addTariff(tariff).then(response => {
            console.log(response);
            this.setState({openDialog:true});
        }).catch(error => {
            console.log(error);
        });

    }

    handleSubmit = event => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;
        if(validation.isValid) {
            this.addTariff();
        } else console.log('not ok');
    }

    cancelAdd = () => {
        this.props.history.push('/manageLocations');
    }

    handleToClose = () => {
        this.setState({openDialog:false});
        this.props.history.push('/manageLocations');
    }

    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return(
            <div className={'AddTariff'} >
            <form>
                <h4 align={'center'}>Add tariff</h4>
                <div className= {validation.season.isInvalid ? 'has-error': undefined}>
                <label>Season:</label>
                <Select name={'season'}
                    value={this.state.season}
                    onChange={this.handleChangeSeason}
                    options={seasons}
                /><span className="help-block" style={{color:'red'}}>{validation.season.message}</span>
                </div><br/>

                <div className= {validation.night.isInvalid ? 'has-error': undefined}>
                <label>Night:</label>
                <Select
                    value={this.state.night}
                    onChange={this.handleChangeNight}
                    options={nightOptions}
                /><span className="help-block" style={{color:'red'}}>{validation.night.message}</span>
                </div><br/>

                <div className= {validation.dayOfWeek.isInvalid ? 'has-error': undefined}>
                <label>Day of week:</label>
                <Select
                    value={this.state.dayOfWeek}
                    onChange={this.handleChangeWeek}
                    options={weekOptions}
                /><span className="help-block" style={{color:'red'}}>{validation.dayOfWeek.message}</span>
                </div><br/>

                <div className= {validation.price.isInvalid ? 'has-error': undefined}>
                    <label> Price: </label>
                    <input className="form-control" name={"price"} onChange={this.handleInputChange} />
                    <span className="help-block" style={{color:'red'}}>{validation.price.message}</span>
                </div><br/>

                <button className="btn btn-primary btn-block button2" onClick={this.cancelAdd}>Cancel</button>
                <button type="Submit" className="btn btn-primary btn-block button1" onClick={this.handleSubmit}>Submit</button>
                <Dialog open={this.state.openDialog}>
                    <DialogContent>
                        <DialogContentText>
                            The tariff was added.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToClose}
                                color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </form>
            </div>

        )
    }


}export default withRouter(AddTariff)