import React, { Component } from "react";
import './ManageLocations.css'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css'
import ManageLocationsService from "../../services/ManageLocationsService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import { Button, DialogActions } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import swal from 'sweetalert'

export default class ManageLocations extends Component {

    constructor(props) {
        super(props)

        this.state = {
            locations: [],
            locationDetails: '',
            openDialogPay: false,
            idLocationToBeDeteted:''
        }
        this.renderTableHeaderLocations = this.renderTableHeaderLocations.bind(this);
        this.renderTableDataLocations = this.renderTableDataLocations.bind(this);

        //locations functions
        this.handleUpdateLocation = this.handleUpdateLocation.bind(this);
        this.handleDeleteLocation = this.handleDeleteLocation.bind(this);
        this.handleViewLocation = this.handleViewLocation.bind(this);
        this.handleAssignCourt = this.handleAssignCourt.bind(this);
        this.handleAddTariff = this.handleAddTariff.bind(this);
        this.handleToClose = this.handleToClose.bind(this);
        this.deleteLocation = this.deleteLocation.bind(this);
    }

    //header pentru tabelul de locatii
    renderTableHeaderLocations() {
        let headerElement = ['Name', 'Address', 'Actions']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    handleUpdateLocation = event => {
        this.props.history.push("/manageLocations/updateLocation/" + event.target.id)
    }

    deleteLocation() {
        ManageLocationsService.deleteLocationById(this.state.idLocationToBeDeteted).then(response => {
            console.log('response => ' + JSON.stringify(response));
        });
        console.log("id-ul locatiei= ",this.state.idLocationToBeDeteted)
        swal("Success", "Location deleted", "success");
        this.setState({ openDialogPay: false });
        window.location.reload(false);
    }

    handleDeleteLocation = event => {
        // this.props.history.push("/manageLocations/deleteLocation/" + event.target.id);
        event.preventDefault();
        this.setState({ openDialogPay: true })
        this.setState({idLocationToBeDeteted: event.target.id})
    }

    handleViewLocation = event => {
        this.props.history.push("/manageLocations/locationDetails/" + event.target.id)
    }

    handleAssignCourt = event => {
        console.log(event.target.id)

        //obtinerea adresei locatiei
        this.props.history.push("/manageLocations/addCourt/" + event.target.id)
    }

    handleAddTariff = event => {
        this.props.history.push("/addTariff/" + event.target.id)
    }

    componentDidMount() {
        ManageLocationsService.getAllLocations().then(response => {
            this.setState({ locations: response.data })
        });
    }

    //corpul tabelului
    renderTableDataLocations() {

        return this.state.locations.map(locatie => {
            const { idLocation, address, name } = locatie //destructuring
            return (
                <tr key={idLocation}>
                    <td >{name}</td>
                    <td>{address}</td>

                    <td>
                        <button type="button" id={locatie.idLocation} onClick={this.handleUpdateLocation} className="btn btn-success" style={{ marginRight: "2%" }}>Update</button>
                        <button type="button" id={locatie.idLocation} onClick={this.handleDeleteLocation} className="btn btn-danger" style={{ marginRight: "2%" }}>Delete</button>
                        <button type="button" id={locatie.idLocation} onClick={this.handleViewLocation} className="btn btn-primary" style={{ marginRight: "2%" }}>View</button>
                        <button type="button" id={locatie.address} onClick={this.handleAssignCourt} className="btn btn-dark" style={{ marginRight: "2%" }}>Assign Court</button>
                        <button type="button" id={locatie.address} onClick={this.handleAddTariff} className="btn btn-dark" style={{ marginRight: "2%" }}>Add Tariff</button>
                    </td>
                </tr>
            )
        })

    }

    handleToClose = () => {
        this.setState({ openDialogPay: false });
        window.location.reload(false);
    }

    render() {

        return (
            <div>
                <h1 align={'center'} style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Manage Locations</h1>

                <button type="button" style={{ marginLeft: "10%", marginTop: "3%" }} className="btn btn-primary" onClick={e => this.props.history.push("/manageLocations/addLocation")}>Add Location</button>

                <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                    <tbody>
                        <tr>{this.renderTableHeaderLocations()}</tr>
                        {this.renderTableDataLocations()}
                    </tbody>
                </table>

                <Dialog open={this.state.openDialogPay}>
                    <DialogContent>
                        <DialogContentText>
                            <h1 align={'center'}>Do you want to delete this location?</h1>

                            <div className="container">
                                <div style={{ marginTop: "3%" }} className="row">
                                    <button onClick={this.deleteLocation} style={{ width: "20%", marginLeft: "37%" }} className="btn btn-primary">YES</button>
                                </div>
                                <div style={{ marginTop: "3%" }} className="row">
                                    <button onClick={this.handleToClose} style={{ width: "20%", marginLeft: "37%" }} className="btn btn-primary">NO</button>
                                </div>
                                <div style={{ marginTop: "3%" }} className="row">

                                </div>
                            </div>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToClose}
                            color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
