import React, { Component } from "react";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css'
import ManageLocationsService from "../../services/ManageLocationsService";


let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    marginLeft: '20%',
    width: '60%'
};


export default class LocationDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            locationDetails: '',
            id: this.props.match.params.id,
        }
    }

    componentDidMount() {
        ManageLocationsService.getLocationById(this.state.id).then(response => {
            this.setState({ locationDetails: response.data })
        });
    }

    render() {

        return (
            <div style={{backgroundColor:"white", borderRadius: "5%", width:"30%", height:"450%", marginLeft:"35%", marginTop:"5%",opacity:"0.76"}}>
            
                <form >
                    <h4 align={'center'} style={{ padding: '2%' }}>Location Details </h4>

                    <b style={{ marginLeft: '42%' }}>Latitude: </b>
                    <input style={inputStyle} type="string" className="form-control" name="first_name" defaultValue={this.state.locationDetails.latitude} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '41%' }}>Longitude: </b>
                    <input style={inputStyle} type="string" className="form-control" name="last_name" defaultValue={this.state.locationDetails.longitude} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '42%' }}> Address: </b>
                    <input style={inputStyle} type="string" className="form-control" name="address" defaultValue={this.state.locationDetails.address} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '44%' }}>Name: </b>
                    <input style={inputStyle} className="form-control" readOnly={true} value={this.state.locationDetails.name} />


                    <b style={{ marginLeft: '40%' }}>Start Hour: </b>
                    <input style={inputStyle} className="form-control" readOnly={true} value={this.state.locationDetails.startHour} />


                    <b style={{ marginLeft: '42%' }}>End Hour: </b>
                    <input style={inputStyle} className="form-control" readOnly={true} value={this.state.locationDetails.endHour} />
                    <b style={{marginLeft: '42%'}}/>
                </form>

            </div>
        );
    }
}
