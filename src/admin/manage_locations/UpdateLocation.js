import React, { useEffect, useState } from 'react'
import MapPicker from 'react-google-map-picker'
import ManageLocationsService from '../../services/ManageLocationsService';
import { useParams } from 'react-router-dom';
import { withRouter } from 'react-router'
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown'
import './ManageLocations.css'
import swal from 'sweetalert'

const DefaultLocation = { lat: 46.770439, lng: 23.591423 };
const DefaultZoom = 10;

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

const UpdateLocation = (props) => {

    const [defaultLocation, setDefaultLocation] = useState(DefaultLocation);

    const [location, setLocation] = useState({});
    const [zoom, setZoom] = useState(DefaultZoom);

    //detalii teren
    const [address, setAddress] = useState('');
    const [name, setName] = useState('');
    const [startHour, setStartHour] = useState(0);
    const [endHour, setEndHour] = useState(0);

    //id locatie
    const { id } = useParams();

    const [locationDetails, setLocationDetails] = useState({})

    useEffect(() => {
        console.log(location)
    }, [location]);

    function handleInitializeDetails() {
        ManageLocationsService.getLocationById(id).then(response => {
            setLocationDetails(response.data);
            setAddress(response.data.address);
            setName(response.data.name);
            setStartHour(response.data.startHour);
            setEndHour(response.data.endHour);
            setLocation({ lat: response.data.latitude, lng: response.data.longitude });
            setDefaultLocation({ lat: response.data.latitude, lng: response.data.longitude });
        });
    }

    function handleChangeLocation(lat, lng) {
        setLocation({ lat: lat, lng: lng });
    }

    function handleChangeZoom(newZoom) {
        setZoom(newZoom);
    }

    const validation = e => {
        if (address === '') {
            swal("Error","Address is empty", "error");
            return false;
        } else
        if (address.length < 3 || !String(address).match(/[a-zA-Z0-9\s]{3,}(.)? (\d+)/)) {
            swal("Error","Invalid address format", "error");
            return false;
        } else
        if (name === '' || name.length < 3) {
            swal("Error","Name is empty/ Minimum length is 4", "error");
            return false;
        } else
        if (!String(name).match(/[A-Z]/)) {
            swal("Error", "Invalid name format", "error");
            return false;
        } else
        if (startHour === '') {
            swal("Error","Start hour is empty", "error");
            return false;
        } else
        if (!String(startHour).match(/^[0-9]+/) || startHour >= 25) {
            swal("Error","Start hour is must be a number between 1-> 24", "error");
            return false;
        } else
        if (endHour === '') {
            swal("Error","Start hour is empty", "error");
            return false;
        } else
        if (!String(endHour).match(/^[0-9]+/) || endHour >= 25) {
            swal("Error","End hour is must be a number between 1-> 24", "error");
            return false;
        }
        return true;
    };

    function handleUpdateLocation() {
        console.log(location);

        let locationToUpdate = {
            latitude: location.lat,
            longitude: location.lng,
            address: address,
            name: name,
            startHour: startHour,
            endHour: endHour
        };
        console.log('location to update => ' + JSON.stringify(locationToUpdate));

        if(validation()) {
            ManageLocationsService.updateLocationById(id, locationToUpdate).then(response => {
                console.log('user => ' + JSON.stringify(response));
                swal("Success", "Location updated", "success");
                props.history.push('/manageLocations');
            });
        }
    }

    const handleSelectStartHour = (e) => {
        setStartHour(e)
    }

    const handleSelectEndHour = (e) => {
        setEndHour(e)
    }

    return (
        <div>
            <h1 align={'center'} style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Update Location</h1>
            <div className='container'>
                <div className='row'>
                    <div className='col-sm'>
                        <label style={{ color: "white" }}>Latitude:</label><input type='text' value={location.lat} disabled />
                        <label style={{ color: "white" }}>Longitude:</label><input type='text' value={location.lng} disabled />

                        <MapPicker defaultLocation={defaultLocation}
                                   center={defaultLocation}
                                   zoom={zoom}
                                   mapTypeId="roadmap"
                                   style={{ height: '500px', width: "500px", marginLeft: "1%", marginTop: "2%" }}
                                   onChangeLocation={handleChangeLocation}
                                   onChangeZoom={handleChangeZoom}
                                   apiKey='AIzaSyD07E1VvpsN_0FvsmKAj4nK9GnLq-9jtj8' />

                    </div>

                    <div className='col-sm Location'>
                        <button type="submit" className="btn btn-primary btn-block" style={{ marginLeft: "35%", marginTop: "2%" }} onClick={handleInitializeDetails}>Initialize Details</button>
                        <div className="mb-3">
                            <label style={{ fontWeight: "bold", color: "white" }}>Address</label>
                            <input style={inputStyle} type="text" className="form-control" placeholder="Enter address" defaultValue={locationDetails.address} onChange={e => setAddress(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label style={{ fontWeight: "bold", color: "white" }}>Name</label>
                            <input style={inputStyle} type="text" className="form-control" placeholder="Enter name of location" defaultValue={locationDetails.name} onChange={e => setName(e.target.value)} />
                        </div>
                        <div className="mb-3">
                            <label style={{ fontWeight: "bold", color: "white" }}>Start Hour:</label>
                            <DropdownButton

                                title={startHour}
                                id="dropdown-size-small"
                                onSelect={handleSelectStartHour}
                                style={{ marginRight: "106%", maxHeight: "28px" }}

                            >
                                <Dropdown.Item eventKey="6">6</Dropdown.Item>
                                <Dropdown.Item eventKey="7">7</Dropdown.Item>
                                <Dropdown.Item eventKey="8">8</Dropdown.Item>
                                <Dropdown.Item eventKey="9">9</Dropdown.Item>
                                <Dropdown.Item eventKey="10">10</Dropdown.Item>
                                <Dropdown.Item eventKey="11">11</Dropdown.Item>
                                <Dropdown.Item eventKey="12">12</Dropdown.Item>
                                <Dropdown.Item eventKey="13">13</Dropdown.Item>
                                <Dropdown.Item eventKey="14">14</Dropdown.Item>
                                <Dropdown.Item eventKey="15">15</Dropdown.Item>
                                <Dropdown.Item eventKey="16">16</Dropdown.Item>
                                <Dropdown.Item eventKey="17">17</Dropdown.Item>
                                <Dropdown.Item eventKey="18">18</Dropdown.Item>
                                <Dropdown.Item eventKey="19">19</Dropdown.Item>
                                <Dropdown.Item eventKey="20">20</Dropdown.Item>
                                <Dropdown.Item eventKey="21">21</Dropdown.Item>
                                <Dropdown.Item eventKey="22">22</Dropdown.Item>
                                <Dropdown.Item eventKey="23">23</Dropdown.Item>
                            </DropdownButton>
                        </div>
                        <div className="mb-3">
                            <label style={{ fontWeight: "bold", color: "white" }}>End Hour:</label>

                            <DropdownButton

                                title={endHour}
                                id="dropdown-size-small"
                                onSelect={handleSelectEndHour}
                                style={{ marginRight: "106%" }}
                            >
                                <Dropdown.Item eventKey="6">6</Dropdown.Item>
                                <Dropdown.Item eventKey="7">7</Dropdown.Item>
                                <Dropdown.Item eventKey="8">8</Dropdown.Item>
                                <Dropdown.Item eventKey="9">9</Dropdown.Item>
                                <Dropdown.Item eventKey="10">10</Dropdown.Item>
                                <Dropdown.Item eventKey="11">11</Dropdown.Item>
                                <Dropdown.Item eventKey="12">12</Dropdown.Item>
                                <Dropdown.Item eventKey="13">13</Dropdown.Item>
                                <Dropdown.Item eventKey="14">14</Dropdown.Item>
                                <Dropdown.Item eventKey="15">15</Dropdown.Item>
                                <Dropdown.Item eventKey="16">16</Dropdown.Item>
                                <Dropdown.Item eventKey="17">17</Dropdown.Item>
                                <Dropdown.Item eventKey="18">18</Dropdown.Item>
                                <Dropdown.Item eventKey="19">19</Dropdown.Item>
                                <Dropdown.Item eventKey="20">20</Dropdown.Item>
                                <Dropdown.Item eventKey="21">21</Dropdown.Item>
                                <Dropdown.Item eventKey="22">22</Dropdown.Item>
                                <Dropdown.Item eventKey="23">23</Dropdown.Item>
                            </DropdownButton>
                        </div>
                        <button type="submit" className="btn btn-primary btn-block" style={{ marginLeft: "35%", marginTop: "2%" }} onClick={() => handleUpdateLocation()}>Update Location</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default withRouter(UpdateLocation)