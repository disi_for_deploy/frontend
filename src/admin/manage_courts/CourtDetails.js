import React, { Component } from "react";
import './ManageCourts.css'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css'
import ManageCourtsService from "../../services/ManageCourtsService";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    marginLeft: '10%',
    width: '80%'
};

export default class CourtDetails extends Component {

    constructor(props) {
        super(props)

        this.state = {
            courtDetails: '',
            id: this.props.match.params.id,
        }
    }

    componentDidMount() {
        ManageCourtsService.getCourtById(this.state.id).then(response => {
            this.setState({ courtDetails: response.data })
        });
    }

    render() {

        return (
            <div style={{ backgroundColor: "white", borderRadius: "5%", width: "20%", height: "450%", marginLeft: "38%", marginTop: "5%" , opacity:"0.76"}}>

                <form >
                    <h4 align={'center'} style={{ padding: '2%' }}>Court Details </h4>

                    <b style={{ marginLeft: '10%' }}>Court Number: </b>
                    <input style={inputStyle} type="string" className="form-control" name="first_name" defaultValue={this.state.courtDetails.courtNumber} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '10%' }}>Type: </b>
                    <input style={inputStyle} type="string" className="form-control" name="last_name" defaultValue={this.state.courtDetails.type} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '10%' }}> Location Address: </b>
                    <input style={inputStyle} type="string" className="form-control" name="address" defaultValue={this.state.courtDetails.locationAddress} onChange={this.handleInputChange} />

                    <b style={{ marginLeft: '10%' }}>Details: </b>
                    <textarea style={inputStyle} className="form-control" readOnly={true} value={this.state.courtDetails.details} />
                    <b style={{ marginLeft: '10%' }}> </b>
                </form>

            </div>
        );
    }
}
