import React, { Component } from "react";
import './ManageCourts.css'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css'
import ManageCourtsService from "../../services/ManageCourtsService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import { Button, DialogActions } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import swal from 'sweetalert';

export default class ManageCourts extends Component {

    constructor(props) {
        super(props)

        this.state = {
            courts: [],
            locationDetails: '',
            openDialogPay: false,
            idCourtToBeDeteted: ''
        }
        this.renderTableHeaderCourts = this.renderTableHeaderCourts.bind(this);
        this.renderTableDataCourts = this.renderTableDataCourts.bind(this);

        //locations functions
        this.handleUpdateLocation = this.handleUpdateLocation.bind(this);
        this.handleDeleteCourt = this.handleDeleteCourt.bind(this);
        this.handleViewLocation = this.handleViewLocation.bind(this);
        this.deleteCourt = this.deleteCourt.bind(this);
    }

    //header pentru tabelul de locatii
    renderTableHeaderCourts() {
        let headerElement = ['Location Address', 'Court Number', 'Actions']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    deleteCourt() {
        ManageCourtsService.deleteCourtById(this.state.idCourtToBeDeteted).then(response => {
            console.log('response => ' + JSON.stringify(response));
        });
        swal("Success", "Court Deleted", "success");
        this.setState({ openDialogPay: false });
        window.location.reload(false);
    }

    handleUpdateLocation = event => {
        this.props.history.push("/manageCourts/updateCourt/" + event.target.id)
    }

    handleDeleteCourt = event => {
        // this.props.history.push("/manageCourts/deleteCourt/" + event.target.id)
        event.preventDefault();
        this.setState({ openDialogPay: true })
        this.setState({ idCourtToBeDeteted: event.target.id })
    }

    handleViewLocation = event => {
        this.props.history.push("/manageCourts/courtDetails/" + event.target.id)
    }

    componentDidMount() {
        ManageCourtsService.getAllCourts().then(response => {
            this.setState({ courts: response.data })
        });
    }

    handleToClose = () => {
        this.setState({ openDialogPay: false });
        window.location.reload(false);
    }


    //corpul tabelului
    renderTableDataCourts() {

        return this.state.courts.map(court => {
            const { idCourt, courtNumber, locationAddress } = court //destructuring
            return (
                <tr key={idCourt}>
                    <td>{locationAddress}</td>
                    <td >{courtNumber}</td>

                    <td>
                        <button type="button" id={court.idCourt} onClick={this.handleUpdateLocation} className="btn btn-success" style={{ marginRight: "2%" }}>Update</button>
                        <button type="button" id={court.idCourt} onClick={this.handleDeleteCourt} className="btn btn-danger" style={{ marginRight: "2%" }}>Delete</button>
                        <button type="button" id={court.idCourt} onClick={this.handleViewLocation} className="btn btn-primary" style={{ marginRight: "2%" }}>View</button>
                    </td>
                </tr>
            )
        })

    }

    render() {

        return (
            <div>
                <h1 align={'center'} style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Manage Courts</h1>

                <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                    <tbody>
                        <tr>{this.renderTableHeaderCourts()}</tr>
                        {this.renderTableDataCourts()}
                    </tbody>
                </table>

                <Dialog open={this.state.openDialogPay}>
                    <DialogContent>
                        <DialogContentText>
                            <h1 align={'center'} >Do you want to delete this court?</h1>

                            <div className="container">
                                <div style={{ marginTop: "3%" }} className="row">
                                    <button onClick={this.deleteCourt} style={{ width: "20%", marginLeft: "37%" }} className="btn btn-primary">YES</button>
                                </div>
                                <div style={{ marginTop: "3%" }} className="row">
                                    <button onClick={this.handleToClose} style={{ width: "20%", marginLeft: "37%" }} className="btn btn-primary">NO</button>
                                </div>
                                <div style={{ marginTop: "3%" }} className="row">
                                </div>
                            </div>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleToClose}
                            color="primary" autoFocus>
                            Close
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}
