import React, { Component } from "react";
import './ManageCourts.css'
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import moment from 'moment';
import ManageCourtsService from "../../services/ManageCourtsService";
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown'
import swal from 'sweetalert'

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

class UpdateCourt extends Component {

    constructor(props) {
        super(props);
        this.validator = new RegisterLoginValidator([{
            field: 'courtNumber',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter court number'
        }, {
            field: 'courtNumber',
            method: this.onlyNumbers,
            validWhen: true,
            message: '* Court number should contains only digits.'
        }, {
            field: 'courtType',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter court type.'
        }, {
            field: 'description',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter description.'
        },  {
            field: 'description',
            method: this.minLength,
            validWhen: true,
            message: '* Address should have length > 5 .'
        }]);

        this.state = {
            courtNumber: '',
            locationAddress: '',
            courtType: '',
            description: '',
            validation: this.validator.valid(),
            id: this.props.match.params.id
        }
        this.submitted = true;
        this.state.maxDate = moment().subtract(18, "years").format('YYYY-MM-DD');

        this.handleSelect = this.handleSelect.bind(this);

    }

    minLength(val) { return (val.length > 5)}

    onlyNumbers(myString) { return /^[0-9]+/.test(myString) }

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleFormSubmit = event => {
        if(this.state.courtType === ''){
            swal("Error", "Court type isn't selected!", "error");
        }

        const validation = this.validator.validate(this.state);

        let court = {
            courtNumber: this.state.courtNumber,
            type: this.state.courtType,
            details: this.state.description,
            locationAddress: this.state.locationAddress
        };
        console.log('court => ' + JSON.stringify(court));

        if(validation.isValid) {
            ManageCourtsService.updateCourtById(this.state.id, court).then(response => {
                console.log(response);
                swal("Success", "Court Updated", "success");
                this.props.history.push("/manageCourts");
            });
        }
    }

    componentDidMount(){
        ManageCourtsService.getCourtById(this.state.id).then(response => {
            this.setState({courtNumber: response.data.courtNumber})
            this.setState({courtType: response.data.type})
            this.setState({description: response.data.details})
            this.setState({locationAddress: response.data.locationAddress})
        });
    }

    handleSelect = e => {
        this.setState({ courtType: e })
    }

    render() {

        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation
        return (

            <div className="Location">
                <form>
                    <h4 align={'center'} style={{fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Update Court</h4>
                    <div className={validation.courtNumber.isInvalid ? 'has-error' : undefined}>
                        <label>Court Number</label>
                        <input style={inputStyle} type="string" className="form-control" name="courtNumber" defaultValue={this.state.courtNumber} onChange={this.handleInputChange} />
                        <span className="help-block" style={{ color: 'red' }}>{validation.courtNumber.message}</span>
                    </div>

                    <DropdownButton
                        title={this.state.courtType}
                        id="dropdown-menu-align-right"
                        onSelect={this.handleSelect}
                        style={{ marginRight: "106%" }}
                    >
                        <Dropdown.Item eventKey="Grass">Grass</Dropdown.Item>
                        <Dropdown.Item eventKey="Clay">Clay</Dropdown.Item>
                        <Dropdown.Item eventKey="Hard">Hard</Dropdown.Item>
                    </DropdownButton>
                    <div className={validation.description.isInvalid ? 'has-error' : undefined}>
                        <label>Description</label>
                        <textarea style={inputStyle} rows="3" type="string" className="form-control" name="description" defaultValue={this.state.description} onChange={this.handleInputChange} />
                        <span className="help-block" style={{ color: 'red' }}>{validation.description.message}</span>
                    </div>
                </form>

                <button onClick={this.handleFormSubmit} type="submit" className="btn btn-primary btn-block">Update Court</button>

            </div>
        );
    }
}
export default UpdateCourt;