import React, { Component } from "react";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import './HomePageAdmin.css'
import '@fortawesome/react-fontawesome'

export default class HomePageAdmin extends Component {


    render() {
        return (
            <div className="HomePage">
                <div className="box" style={{marginLeft: "25%", marginTop: "10%"}}>
                    <div className="container">
                        <div className="row">

                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                <div className="box-part text-center">

                                <i className="fa fa-bolt fa-3x" aria-hidden="true"/>

                                    <div className="title">
                                        <h4>Manage Locations</h4>
                                    </div>

                                    <button onClick={e => this.props.history.push("/manageLocations")} className="button-81">Operations</button>

                                </div>
                            </div>

                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                                <div className="box-part text-center">

                                <i className="fa fa-bolt fa-3x" aria-hidden="true"/>

                                    <div className="title">
                                        <h4>Manage Courts</h4>
                                    </div>

                                    <button onClick={e => this.props.history.push("/manageCourts")} className="button-81">Operations</button>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
