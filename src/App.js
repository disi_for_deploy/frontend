import './App.css';
import React from 'react'

import Home from './components/home/Home';
import Navigationbar from './components/navbar/Navbar';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Login from './components/login/Login';
import Register from './components/register/Register';
import ResetPasswordEmail from './components/reset_password/ResetPasswordEmail';
import ResetPasswordCode from './components/reset_password/ResetPasswordCode';
import ManageLocations from './admin/manage_locations/ManageLocations';
import AddLocation from './admin/manage_locations/AddLocation';
import LocationDetails from './admin/manage_locations/LocationDetails';
import CourtDetails from './admin/manage_courts/CourtDetails';
import UpdateLocation from './admin/manage_locations/UpdateLocation';
import AddCourt from './admin/manage_locations/AddCourt';
import ManageCourts from './admin/manage_courts/ManageCourts';
import UpdateCourt from './admin/manage_courts/UpdateCourt';
import HomePageAdmin from './admin/HomePageAdmin';
import Client from './client/clientPage/Client';
import AddTariff from "./admin/add_tariff/AddTariff";
import Locations from './components/locations_courts/Locations'
import Courts from './components/locations_courts/Courts';

import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom';
import Wallet from "./client/wallet/Wallet";
import MyReservations from "./client/reservations/MyReservations";
import MySubscriptions from "./client/subscriptions/MySubscriptions";
import ViewReservation from "./client/reservations/ViewReservation";
import CourtDisponibility from './components/court_disponibility/CourtDisponibility';
import ReservationSummary from "./client/reservations/ReservationSummary";
import MakeSubscription from "./client/subscriptions/MakeSubscription";
import SubscriptionSummary from "./client/subscriptions/SubscriptionSummary";
import MakeReservation from "./client/reservations/MakeReservation";
import Notifications from "./client/notifications/Notifications";
import JoinReservation from "./client/joinReservation/JoinReservation";

class App extends React.Component {

    render() {

        return (
            <div>
                <Router>
                    <Navigationbar />
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/login" exact component={Login}/>
                        <Route path="/register" exact component={Register}/>
                        <Route path="/locations" exact component={Locations}/>
                        <Route path="/location/:id" exact component={Courts}/>
                        <Route path="/resetPasswordEmail" exact component={ResetPasswordEmail}/>
                        <Route path="/resetPasswordCode/:email" exact component={ResetPasswordCode}/>
                        <Route path="/manageLocations" exact component={ManageLocations}/>
                        <Route path="/manageLocations/addLocation" exact component={AddLocation}/>
                        <Route path="/manageLocations/addCourt/:id" exact component={AddCourt}/>
                        <Route path="/manageLocations/locationDetails/:id" exact component={LocationDetails}/>
                        <Route path="/manageLocations/updateLocation/:id" exact component={UpdateLocation}/>
                        <Route path="/manageCourts" exact component={ManageCourts}/>
                        <Route path="/manageCourts/courtDetails/:id" exact component={CourtDetails}/>
                        <Route path="/manageCourts/updateCourt/:id" exact component={UpdateCourt}/>
                        <Route path="/adminHomePage" exact component={HomePageAdmin}/>
                        <Route path="/client/:username" exact component={Client}/>
                        <Route path="/addTariff/:id" exact component={AddTariff}/>
                        <Route path="/wallet/:username" exact component={Wallet}/>
                        <Route path="/myReservations/:username" exact component={MyReservations}/>
                        <Route path="/mySubscriptions/:username" exact component={MySubscriptions}/>
                        <Route path="/joinReservation/:username" exact component={JoinReservation}/>
                        <Route path="/myReservation/viewReservation/:id" exact component={ViewReservation}/>
                        <Route path="/courtDisponibility/:id" exact component={CourtDisponibility}/>
                        <Route path="/makeReservation/:id" exact component={MakeReservation}/>
                        <Route path="/reservationSummary/:username" exact component={ReservationSummary}/>
                        <Route path="/makeSubscription/:id" exact component={MakeSubscription}/>
                        <Route path="/subscriptionSummary/:username" exact component={SubscriptionSummary}/>
                        <Route path="/notifications" exact component={Notifications}/>
                        <Route path="/joinReservation/:username" exact component={JoinReservation}/>
                    </Switch>
                </Router>
            </div>
        )
    };
}

export default App
