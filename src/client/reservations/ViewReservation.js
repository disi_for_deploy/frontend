import React, {Component} from "react";
import {withRouter} from "react-router";
import './ViewReservation.css'
import ReservationService from "../../services/ReservationService";
import ManageCourtsService from "../../services/ManageCourtsService";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    marginLeft: '25%',
    width: '50%',
    textAlign:'center'
};

class ViewReservation extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            reservation: '',
            location: '',
            courtId: '',
            courtNb: ''
        }

        this.getCourt = this.getCourt.bind(this);
        this.getReservation= this.getReservation.bind(this);
    }

    componentDidMount() {
        this.getReservation(this.state.id)
    }

    getReservation(id){
        if(id!= null) {
            ReservationService.getReservationById(id).then(response => {
                if(response.data != null) {
                    this.setState({reservation: response.data})
                    this.setState({courtId: response.data.courtId})
                    this.getCourt(response.data.courtId)
                }

            })
        }
    }

    getCourt(id){
        if(id != null) {
            ManageCourtsService.getCourtById(id).then(response => {
                console.log(response.data)
                this.setState({location: response.data.locationAddress});
                this.setState({courtNb: response.data.courtNumber});
            });
        }
    }

    render() {
        return(
            <div className={'ViewReservation'}>
                <form>
                   <h4 align={'center'} style={{padding: '2%'}}>Reservation Details </h4>

                        <b style={{marginLeft: '44%'}}>Date: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.reservation.date} />

                        <b style={{marginLeft: '41%'}}>Start time: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.reservation.startTime} />

                        <b style={{marginLeft: '42%'}}>End time: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.reservation.endTime} />

                        <b style={{marginLeft: '41%'}}>Total price: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.reservation.totalPrice} />

                        <b style={{marginLeft: '43%'}}>Location: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.location} />

                        <b style={{marginLeft: '40%'}}>Court number: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.courtNb} />

                        <br/>
                </form>

            </div>
        )
    }


}export default withRouter(ViewReservation)