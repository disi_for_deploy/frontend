import React, {Component} from "react";
import {withRouter} from "react-router";
import './ReservationSummary.css'
import ReservationService from "../../services/ReservationService";
import ManageCourtsService from "../../services/ManageCourtsService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {Button, DialogActions} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import moment from "moment";

class ReservationSummary extends Component{

    constructor(props) {
        super(props);
        this.state = {
            totalPrice:'',
            reservation: JSON.parse(localStorage.getItem("reservation")),
            client: JSON.parse(sessionStorage.getItem("client")),
            courtNumber: '',
            location: '',
            openDialogPay: false,
            okReservation: ''
        }
    }

    componentDidMount(){
        ReservationService.computePrice(this.state.reservation).then(response =>{
            this.setState({totalPrice:response.data}, function () {
                console.log(this.state.totalPrice)
            })
        })

        ManageCourtsService.getCourtById(this.state.reservation.courtId).then(response =>{
            this.setState({courtNumber:response.data.courtNumber})
            this.setState({location:response.data.locationAddress})
        })
    }

    verifyReservation(){
        let ok = 1;
        ReservationService.getReservations().then(response => {
            response.data.map(reservation =>{
                if(reservation.username === this.state.client.username){
                    if(moment(reservation.date).subtract(1,'month').add(reservation.endTime, 'hours').toISOString() > moment().toISOString()){
                        this.setState({okReservation:false});
                        ok = 0;
                    }
                }
                return reservation
            })

            if(ok === 1){
                this.setState({okReservation:true});
                ReservationService.postReservation(this.state.reservation).then(response =>{
                    console.log(response.data)
                })
            }
        });
    }

    handleCancel = (event) => {
        event.preventDefault();
        localStorage.removeItem("reservation");
        this.props.history.push('/');
    }

    handlePay = (event) => {
        event.preventDefault();
        console.log(this.state.reservation);
        this.verifyReservation();
        this.setState({openDialogPay: true})

    }

    handleToClose = () => {
        localStorage.removeItem("reservation");
        this.setState({openDialogPay: false});
        this.props.history.push('/myReservations/' + this.state.client.username);
        window.location.reload(false);
    }

    render() {
        return(
            <div className={'ReservationSummary'}>
                <form>
                    <fieldset>
                        <h4 className={'text1'}>Reservation Summary</h4><br/>
                        <p className={'info'}><i className="fa fa-user"/> Name: <b>{this.state.client.firstName+" "+this.state.client.lastName}</b></p>
                        <p className={'info'}><i className="fa fa-th"/> Court number: <b>{this.state.courtNumber}</b></p>
                        <p className={'info'}><i className="fa fa-map-marker"/> Location address: <b>{this.state.location}</b></p>
                        <p className={'info'}><i className="fa fa-calendar-check-o"/> Date: <b>{moment(this.state.reservation.date).format('DD/MM/YYYY')}</b></p>
                        <p className={'info'}><i className="fa fa-clock-o"/> Start time: <b>{this.state.reservation.startTime+":00"}</b></p>
                        <p className={'info'}><i className="fa fa-clock-o"/> End time: <b>{this.state.reservation.endTime+":00"}</b></p>
                        <p className={'info'}><i className="fa fa-dollar" style={{marginLeft:'1%'}}/> Total price: <b>{this.state.totalPrice}</b></p>
                    </fieldset><br/>

                    <button className="btn btn-danger" style={{ marginLeft: "1%", marginRight: "35%" }} onClick={this.handleCancel}>CANCEL</button>
                    <button className="btn btn-success" style={{ marginLeft: "10%" }}>Pay with crypto</button>
                    <button className="btn btn-success" style={{ marginLeft: "1%" }} onClick={this.handlePay}>Pay</button>

                    <Dialog open={this.state.openDialogPay}>
                        <DialogContent>
                            <DialogContentText>
                                {this.state.totalPrice > this.state.client.wallet &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                <span>You don't have enough money to make the reservation.</span></div>}
                                {this.state.totalPrice <= this.state.client.wallet && this.state.okReservation === true &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                    <span>Your reservation was made with success.</span></div>}
                                {this.state.totalPrice <= this.state.client.wallet && this.state.okReservation === false &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                    <span>You can not have more than one reservation in the future and you already have one.</span></div>}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleToClose}
                                    color="primary" autoFocus>
                                Close
                            </Button>
                        </DialogActions>
                    </Dialog>

                </form>
            </div>
        )
    }

}export default withRouter(ReservationSummary)