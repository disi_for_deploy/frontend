import React,{Component} from "react";
import {withRouter} from "react-router";
import './MyReservations.css'
import ReservationService from "../../services/ReservationService";
import ClientService from "../../services/ClientService";
import moment from "moment";
import ManageCourtsService from "../../services/ManageCourtsService";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {Button, DialogActions} from "@material-ui/core";
import swal from 'sweetalert';
import SockJsClient from "react-stomp";
import Cookies from 'universal-cookie';
const cookies = new Cookies();
// const SOCKET_URL = 'https://localhost:8080/ws-notif';
const SOCKET_URL = 'https://disiproject2022.herokuapp.com/ws-notif';

class MyReservations extends Component{

    constructor(props) {
        super(props)

        this.state = {
            reservations: [],
            client: JSON.parse(sessionStorage.getItem("client")),
            dateNow: moment().toISOString(),
            username: this.props.match.params.username,
            courts: [],
            cancelDialog: false,
            dateReservation: '',
            cancelOk: '',
            idReservation: '',
            transferOk: false
        }

        this.searchPartner = this.searchPartner.bind(this);
    }

    componentDidMount(){
        ReservationService.getReservations().then(response => {
            this.setState({ reservations: response.data })
        });

        ManageCourtsService.getAllCourts().then(response =>{
            this.setState({ courts: response.data })
        });
    }

    renderTableHeaderReservations() {
        let headerElement = ['Date', 'Start time', 'End time', 'Price', 'Actions']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableHeaderReservationsHistory() {
        let headerElement = ['Date', 'Start time', 'End time', 'Location', 'Court number', 'Price']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    getNumber(id) {
        let nb;
        this.state.courts.map(court => {
            if(court.idCourt === id) { nb = court.courtNumber}
            return nb
        })
        return nb
    }

    getLocation(id) {
        let location;
        this.state.courts.map(court => {
            if(court.idCourt === id) { location = court.locationAddress}
            return location
        })
        return location
    }

    verifyDateReservation(id){
        ReservationService.getReservationById(id).then(response => {
            let now = moment(this.state.dateNow);
            let dateReservation = moment(response.data.date).add(response.data.startTime, 'hours')
            if(dateReservation.diff(now, 'hours') < 24){
                this.setState({cancelOk:false});
            } else this.setState({cancelOk:true});

        })
    }

    handleViewReservation = event => {
        this.props.history.push("/myReservation/viewReservation/" + event.target.id)
    }

    searchPartner = event =>{
        console.log("id-ul rezervarii: ",event.target.id);

        ReservationService.getReservationById(event.target.id).then(response => {
            ReservationService.searchPartner(response.data).then(res => {
                localStorage.setItem("reservation_code", res.data);
                localStorage.setItem("reservation_id",event.target.id);
                swal("Success", "Request sent successfully! ", "success");
            });
        });
    }

    handleCancelReservation = event => {
        this.verifyDateReservation(event.target.id);
        this.setState({cancelDialog: true});
        this.setState({idReservation: event.target.id});
    }

    handleCancelSuccess = () => {
        console.log(this.state.idReservation)
        let reservation;
        ReservationService.getReservationById(this.state.idReservation).then(response =>{
            reservation = response.data;
            let data ={
                "date": reservation.date,
                "startTime": reservation.startTime,
                "endTime": reservation.endTime,
                "totalPrice": reservation.totalPrice,
                "username": reservation.username,
                "courtId": reservation.courtId
            }
            ClientService.returnMoney(data).then(response => console.log('1'));
            ReservationService.cancelReservation(this.state.idReservation).then(response => console.log(response.data));
            this.componentDidMount();
        })

        this.setState({cancelDialog: false});
        this.props.history.push('/myReservations/' + this.state.client.username);
    }

    handleCancelAgree = () => {
        ReservationService.cancelReservation(this.state.idReservation).then();
        this.setState({cancelDialog: false});
        this.componentDidMount();
        this.props.history.push('/myReservations/' + this.state.client.username);
    }

    onConnect = () => {
        console.log("Connected through WebSockets!")
    }

    onDisconnect = () => {
        console.log("Disconnected from WebSockets!")
    }

    onMessageReceived = (msg) => {
        let data = {
            title: msg['title'],
            description: msg['details'],
            reservationId: msg['idReservation'],
            username: msg['username']
        }

        if(this.state.transferOk === true)
            cookies.set("Transfer_partner", data, { path: '/' });
        console.log("Received: " + msg)
    }

    handleTransfer = () =>{
        let reservation;
        ReservationService.getReservationById(this.state.idReservation).then(response =>{
            reservation = response.data;
            let data ={
                "idReservation": this.state.idReservation,
                "date": reservation.date,
                "startTime": reservation.startTime,
                "endTime": reservation.endTime,
                "totalPrice": reservation.totalPrice,
                "username": reservation.username,
                "courtId": reservation.courtId
            }
            ReservationService.transferReservation(data).then(response =>{
                console.log(response.data)
                localStorage.setItem("transferCode", response.data);
                localStorage.setItem("contor", 0);
                localStorage.setItem("courtId", reservation.courtId);
                localStorage.setItem("idReservation", this.state.idReservation);
            })
        })
        this.setState({cancelDialog: false});
        this.setState({transferOk: true});
        swal("Success", "Request sent successfully! ", "success");
        this.props.history.push('/myReservations/' + this.state.client.username);
    }

    renderTableDataReservations() {
        return this.state.reservations.map(reservation => {
            const { idReservation, date, startTime, endTime, totalPrice, username } = reservation
            return this.state.dateNow <= moment(date).subtract(1, 'M').add(endTime, 'H').toISOString()
            && this.state.username === username ? (
                <tr key={idReservation}>
                    <td >{moment(date).subtract(1, 'M').format('DD-MM-YYYY')}</td>
                    <td>{startTime}</td>
                    <td>{endTime}</td>
                    <td>{totalPrice}</td>

                    <td>
                        <button type="button" id={reservation.idReservation} onClick={this.handleViewReservation} className="btn btn-success" style={{ marginRight: "2%" }}>View reservation</button>
                        <button type="button" id={reservation.idReservation} onClick={this.handleCancelReservation} className="btn btn-danger" style={{ marginRight: "2%" }}>Cancel reservation</button>
                        <button type="button" id={reservation.idReservation} onClick={this.searchPartner} className="btn btn-primary" style={{ marginRight: "2%" }}>Search partner</button>

                    </td>
                </tr>
            ) : undefined
        })

    }

    renderTableDataReservationsHistory() {
        return this.state.reservations.map(reservation => {
            const { idReservation, date, startTime, endTime, totalPrice, courtId, username } = reservation
            return this.state.dateNow > moment(date).subtract(1, 'M').add(endTime, 'H').toISOString()
            && this.state.username === username ?(
                <tr key={idReservation}>
                    <td >{moment(date).subtract(1, 'M').format('DD-MM-YYYY')}</td>
                    <td>{startTime}</td>
                    <td>{endTime}</td>
                    <td>{this.getLocation(courtId)}</td>
                    <td>{this.getNumber(courtId)}</td>
                    <td>{totalPrice}</td>
                </tr>
            ): undefined
        })

    }

    render() {
        return(
            <div className={'MyReservations'}>
                <form>
                    <h4 align={'center'} style={{ padding:'2%' }}>My reservations</h4>
                    <h6 style={{ marginLeft:'11%' }}>ON GOING</h6>
                    <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                        <tbody>
                        <tr>{this.renderTableHeaderReservations()}</tr>
                        {this.renderTableDataReservations()}
                        </tbody>
                    </table><br/>

                    <h6 style={{ marginLeft:'11%' }}>HISTORY</h6>
                    <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                        <tbody>
                        <tr>{this.renderTableHeaderReservationsHistory()}</tr>
                        {this.renderTableDataReservationsHistory()}
                        </tbody>
                    </table><br/>

                    <Dialog open={this.state.cancelDialog}>
                        <DialogContent>
                            <DialogContentText>
                                <span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                {this.state.cancelOk === true && <span>The reservation was canceled with success.</span>}
                                {this.state.cancelOk === false && <span>Your reservation is in less than 24 hours. Do you agree to cancel your reservation without getting a refund?</span>}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            {this.state.cancelOk === true && <Button onClick={this.handleCancelSuccess} color="primary" autoFocus> Close </Button>}
                            {this.state.cancelOk === false &&<div>
                                <Button onClick={this.handleCancelAgree} color="primary" autoFocus>Yes, I agree</Button>
                                <Button onClick={this.handleTransfer} color="primary" autoFocus>Transfer reservation</Button>
                            </div>}
                        </DialogActions>
                    </Dialog>

                    <SockJsClient
                        url={SOCKET_URL}
                        topics={['/notifications']}
                        onConnect={this.onConnect}
                        onDisconnect={this.onDisconnect}
                        onMessage={msg => this.onMessageReceived(msg)}
                        debug={false}
                    />

                </form>
            </div>
        )
    }

}export default withRouter(MyReservations)