import React, {Component} from "react";
import {withRouter} from "react-router";
import './MakeReservation.css'
import moment from 'moment';
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Select from "react-select";
import ReservationService from "../../services/ReservationService";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "black",
    width: "30%",
    float: "left",
    marginLeft: "3%",
    fontSize: "17px",
    marginTop: "3%"
};

class MakeReservation extends Component{
    constructor(props) {
        super(props);
        this.validator = new RegisterLoginValidator([{
            field: 'dateReservation',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select date.'
        }, {
            field: 'startTime',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select start time.'
        }, {
            field: 'endTime',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select end_time.'
        }]);

        this.state = {
            client: JSON.parse(sessionStorage.getItem("client")),
            dateReservation: '',
            validation: this.validator.valid(),
            courtId: this.props.match.params.id,
            dateSelected: false,
            startTimeSelected: false,
            startTime: '',
            endTme: '',
            intervals: [],
            selectStart: [],
            selectEnd: [],
            totalPrice: ''
        }

        this.submitted = false;
        this.dateMin = moment().add(1, 'days').format('YYYY-MM-DD')
    }

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            dateReservation: event.target.value,
        });
        this.setState({selectStart: []}, function () {
            this.getInterval();
        });
        this.setState({dateSelected: true});
    }

    mapStart(){
        console.log(Object.keys(this.state.intervals))
        Object.keys(this.state.intervals).forEach(element =>{
            const newSelect = this.state.selectStart;
            newSelect.push({ value: element, label: element });
            this.setState({selectStart: newSelect});
        })
    }

    mapEnd(start){
        this.state.intervals[start].forEach(element=>{
            console.log(this.state.selectEnd)
            const newSelect = this.state.selectEnd;
            newSelect.push({ value: element, label: element });
            this.setState({selectEnd: newSelect});
        })
    }

    handleStartTimeChange = (startTime) =>{
        this.setState({startTime}, function (){
            console.log(this.state.startTime.value)
            this.setState({selectEnd: []}, function () {
                this.mapEnd(this.state.startTime.value)
            });
        });
        this.setState({startTimeSelected:true});
    }

    handleEndTimeChange = (endTime) =>{
        this.setState({endTime});
    }

    getInterval(){
        let data = {
            "date": this.state.dateReservation,
            "courtId": this.state.courtId
        }

        ReservationService.getAvailableIntervals(data).then(response =>
            this.setState({intervals:response.data}, function () {
                this.mapStart();
            })
        )
    }

    redirectToSummary(){
        let reservation ={
            "date": this.state.dateReservation,
            "startTime": parseInt(this.state.startTime.value, 10),
            "endTime": this.state.endTime.value,
            "username": this.state.client.username,
            "courtId": this.state.courtId
        }
        console.log('reservation => ' + JSON.stringify(reservation));

        localStorage.setItem("reservation", JSON.stringify(reservation));
        this.props.history.push('/reservationSummary/' + this.state.client.username);
    }

    handleSubmit = event => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });

        this.submitted = true;
        if(validation.isValid) {
            this.redirectToSummary();
        } else console.log('not ok');
    }

    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return(
            <div className={'MakeReservation'}>
                <form>
                    <h4 align={'center'} style={{ padding:'2%' }}>Make a reservation</h4>
                    <div style={inputStyle} className={validation.dateReservation.isInvalid ? 'has-error': undefined}>
                        <label>Select date:</label>
                        <input  type="date" min={this.dateMin} className="form-control" name="dateReservation" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.dateReservation.message}</span>
                    </div>
                    <br/>

                    {this.state.dateSelected === true && <div className= {validation.startTime.isInvalid ? 'has-error': undefined}>
                    <div className={'selectStyle1'}><label>Start time:</label>
                        <Select
                        value={this.state.startTime}
                        onChange={this.handleStartTimeChange}
                        options={this.state.selectStart}
                        />
                        <span className="help-block" style={{color:'red'}}>{validation.startTime.message}</span>
                    </div>
                    </div>}

                    {this.state.startTimeSelected === true && <div className= {validation.endTime.isInvalid ? 'has-error': undefined}>
                    <div className={'selectStyle2'}><label>End time:</label>
                        <Select
                            value={this.state.endTime}
                            onChange={this.handleEndTimeChange}
                            options={this.state.selectEnd}
                        />
                        <span className="help-block" style={{color:'red'}}>{validation.endTime.message}</span>
                    </div>
                    </div>}
                    <br style={{ clear:"left"}}/>

                    <button type="Submit" className="btn btn-primary btn-block buttonPrice" onClick={this.handleSubmit}>See price <i className="fa fa-angle-double-right"/></button>

                </form>

            </div>
        )
    };

}export default withRouter(MakeReservation)