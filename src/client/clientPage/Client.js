import React, {Component} from "react";
import './Client.css'
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator";
import moment from "moment";
import img from "../../commons/images/acc.png";
import jwt from "jwt-decode";
import ClientService from "../../services/ClientService";
import swal from "sweetalert";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    marginLeft: '18%',
    width: '30%'
};

class Client extends Component{
    constructor(props) {
        super(props);
        this.validator = new RegisterLoginValidator([{
            field: 'first_name',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter first name.'
        }, {
            field: 'first_name',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* First name should not have spaces.'
        },{
            field: 'first_name',
            method: this.startsWithCapital,
            validWhen: true,
            message: '* First name should start with a capital letter.'
        },  {
            field: 'first_name',
            method: this.onlyLetters,
            validWhen: true,
            message: '* First name should not contain numbers or special characters, except -.'
        }, {
            field: 'first_name',
            method: this.minLengthName,
            validWhen: true,
            message: '* First name should have at least 3 characters.'
        }, {
            field: 'last_name',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* Last name should not have spaces.'
        }, {
            field: 'last_name',
            method: this.startsWithCapital,
            validWhen: true,
            message: '* Last name should start with a capital letter.'
        }, {
            field: 'last_name',
            method: this.onlyLetters,
            validWhen: true,
            message: '* Last name should not contain any number .'
        }, {
            field: 'last_name',
            method: this.minLengthName,
            validWhen: true,
            message: '* Last name should have at least 3 characters.'
        }, {
            field: 'last_name',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter last name.'
        }, {
            field: 'address',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter address.'
        }, {
            field: 'address',
            method: this.onlySpaces,
            validWhen: false,
            message: '* Address is empty.'
        }, {
            field: 'address',
            method: this.addressFormat,
            validWhen: true,
            message: '* Address should have a valid format.'
        }]);

        this.state = {
            first_name: '',
            last_name: '',
            address: '',
            showBirthday: '',
            birthday:'',
            idClient: '',
            username: '',
            role: '',
            validation: this.validator.valid(),

        }
        this.submitted = false;

    }

    minLength(val) { return (val.length > 4)}

    maxLength(val) { return (val.length < 21)}

    containsWhitespace(myString) { return /\s/.test(myString)}

    onlySpaces(myString) { return /^\s*$/.test(myString)}

    minLengthName(val) { return (val.length > 2)}

    onlyLetters(myString) { return /^[a-zA-Z-]+$/.test(myString)}

    startsWithCapital(myString){ return /[A-Z]/.test(myString.charAt(0))}

    addressFormat(myString) { return  /[a-zA-Z0-9\s]{3,}(.)? (\d+)/.test(myString)}

    componentDidMount() {
        this.getUsername()
    }

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    getUsername(){
        if(localStorage.getItem("login_token")===''){
            this.setState({username:''});
        } else {
            let username=jwt(localStorage.getItem("login_token")).sub;
            this.setState({username:username});
            let role=jwt(localStorage.getItem("login_token")).roles[0];
            if(role==="client") {
                this.setState({role:"client"}, function () {
                    this.getClient()
                });
            }
        }
    }
    getClient(){
        if(this.state.username!==''){
            ClientService.getClient(this.state.username).then(response=>{
                this.setState({idClient: response.data.idClient});
                this.setState({first_name: response.data.firstName});
                this.setState({last_name: response.data.lastName});
                this.setState({address: response.data.address});
                this.setState({birthday: response.data.birthday});
                let day = moment(response.data.birthday).format('YYYY-MM-DD');
                this.setState({showBirthday: day});
            });
        }
    }

    updateClient(){

        let client = {
            id: this.state.idClient,
            firstName: this.state.first_name,
            lastName: this.state.last_name,
            address: this.state.address,
            birthday: this.state.birthday,
            username: this.state.username
        };
        console.log('client => ' + JSON.stringify(client));

        ClientService.updateClient(client, this.state.idClient).then(response =>{
            console.log('client => ' + JSON.stringify(response));
            swal("Success", "Profile updated", "success");
            window.location.reload(false);
        })

    }

    handleFormSubmit = event => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;
        if(validation.isValid) {
            this.updateClient();
            console.log('ok');
        } else console.log('not ok');

    }

    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return (

            <div className={'Client'}> {this.state.role==="client" &&
                <form>
                    <h4 align={'center'} style={{ padding:'2%' }}>My profile </h4>
                    <img src={img} alt={'img'}/>
                    <div className= {validation.first_name.isInvalid ? 'has-error': undefined}>
                        <b style={{marginLeft:'2%'}}>First name: </b>
                        <input style={inputStyle} type="string" className="form-control" name="first_name" defaultValue={this.state.first_name} onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red', marginLeft:'3%'}}>{validation.first_name.message}</span>
                    </div>

                    <div className={validation.last_name.isInvalid ? 'has-error': undefined}>
                        <b style={{marginLeft:'2%'}}>Last name: </b>
                        <input style={inputStyle} type="string" className="form-control" name="last_name" defaultValue={this.state.last_name} onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red', marginLeft:'18%'}}>{validation.last_name.message}</span>
                    </div>

                    <div className={validation.address.isInvalid ? 'has-error': undefined}>
                        <b style={{marginLeft:'18%'}}> Address: </b>
                        <input style={inputStyle} type="string" className="form-control" name="address" defaultValue={this.state.address} onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red', marginLeft:'18%'}}>{validation.address.message}</span>
                    </div>

                    <div>
                        <b style={{marginLeft:'18%'}}> Username: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.username} />
                    </div><br/>

                    <div>
                        <b style={{marginLeft:'18%'}}> Birthday: </b>
                        <input style={inputStyle} className="form-control" readOnly={true} value={this.state.showBirthday} />
                    </div>

                    <br/>
                    <button onClick={this.handleFormSubmit} type="submit" className="btn btn-primary btn-block">Submit changes</button>
                    <br/>

                </form>}

            </div>
        );
    }

} export default Client