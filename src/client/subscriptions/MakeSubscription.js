import React, {Component} from "react";
import {withRouter} from "react-router";
import './MakeSubscription.css'
import moment from 'moment';
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import Select from "react-select";
import SubscriptionService from "../../services/SubscriptionService";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "black",
    width: "30%",
    marginLeft: "3%",
    fontSize: "17px"
};

const days = [
    { value: 'monday', label: 'Monday' },
    { value: 'tuesday', label: 'Tuesday' },
    { value: 'wednesday', label: 'Wednesday' },
    { value: 'thursday', label: 'Thursday' },
    { value: 'friday', label: 'Friday' },
    { value: 'saturday', label: 'Saturday' },
    { value: 'sunday', label: 'Sunday' }
];

class MakeSubscription extends Component{

    constructor(props) {
        super(props);
        this.validator = new RegisterLoginValidator([{
            field: 'dateSubscription',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select date.'
        }, {
            field: 'startTime',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select start time.'
        }, {
            field: 'endTime',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select end_time.'
        }]);

        this.state = {
            client: JSON.parse(sessionStorage.getItem("client")),
            dateSubscription: '',
            validation: this.validator.valid(),
            courtId: this.props.match.params.id,
            dateSelected: false,
            startTimeSelected: false,
            startTime: '',
            endTme: '',
            intervals: [],
            selectStart: [],
            selectEnd: [],
            totalPrice: '',
            typeDay: '',
            endDay: ''
        }

        this.submitted = false;
        this.dateMin = moment().add(1, 'days').format('YYYY-MM-DD')
    }

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            dateSubscription: event.target.value }, function () {
            this.setState({endDay: moment(this.state.dateSubscription).add(1,'month').subtract(1,'day').format('DD/MM/YYYY')})
        });
        this.setState({selectStart: []});
        this.setState({dateSelected: true});
    }

    mapStart(){
        console.log(Object.keys(this.state.intervals))
        Object.keys(this.state.intervals).forEach(element =>{
            const newSelect = this.state.selectStart;
            newSelect.push({ value: element, label: element });
            this.setState({selectStart: newSelect});
        })
    }

    mapEnd(start){
        this.state.intervals[start].forEach(element=>{
            console.log(this.state.selectEnd)
            const newSelect = this.state.selectEnd;
            newSelect.push({ value: element, label: element });
            this.setState({selectEnd: newSelect});
        })
    }

    handleStartTimeChange = (startTime) =>{
        this.setState({startTime}, function (){
            console.log(this.state.startTime.value)
            this.setState({selectEnd: []}, function () {
                this.mapEnd(this.state.startTime.value)
            });
        });
        this.setState({startTimeSelected:true});
    }

    handleEndTimeChange = (endTime) =>{
        this.setState({endTime});
    }

    handleDayChange = (typeDay) =>{
        this.setState({typeDay}, function () {
            this.getInterval();
        });
    }

    getInterval(){
        let data = {
            "startDate": this.state.dateSubscription,
            "dayOfWeek": this.state.typeDay.value,
            "courtId": this.state.courtId
        }

        SubscriptionService.getAvailableIntervals(data).then(response =>
            this.setState({intervals:response.data}, function () {
                this.mapStart();
            })
        )
    }

    redirectToSummary(){
        let subscription ={
            "startDate": this.state.dateSubscription,
            "startTime": parseInt(this.state.startTime.value, 10),
            "endTime": this.state.endTime.value,
            "dayOfWeek" :this.state.typeDay.value,
            "username": this.state.client.username,
            "courtId": this.state.courtId
        }
        console.log('Subscription => ' + JSON.stringify(subscription));

        localStorage.setItem("subscription", JSON.stringify(subscription));
        this.props.history.push('/subscriptionSummary/' + this.state.client.username);
    }

    handleSubmit = event => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });

        this.submitted = true;
        if(validation.isValid) {
            this.redirectToSummary();
        } else console.log('not ok');
    }

    render() {

        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation
        return(
            <div className={'MakeSubscription'}>
                <form>
                    <h4 align={'center'} style={{ padding:'2%' }}>Make a subscription</h4>
                    <div style={inputStyle} className={validation.dateSubscription.isInvalid ? 'has-error': undefined}>
                        <label>Select start date:</label>
                        <input  type="date" min={this.dateMin} className="form-control" name="dateSubscription" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.dateSubscription.message}</span>
                    </div>
                    <br/>

                    {this.state.dateSelected === true && <div className={'inputStyle1'}>
                        <label>End date:</label>
                        <input className="form-control" style={{background:"transparent"}} readOnly={true} value={this.state.endDay}/>
                    </div>}<br/>

                    {this.state.dateSelected === true && <div className={'selectDays'}><label>Select day:</label>
                        <Select
                            value={this.state.typeDay}
                            onChange={this.handleDayChange}
                            options={days}
                        />
                    </div>}

                    {this.state.typeDay !== '' && <div className= {validation.startTime.isInvalid ? 'has-error': undefined}>
                        <div className={'selectStyle1'}><label>Start time:</label>
                            <Select
                                value={this.state.startTime}
                                onChange={this.handleStartTimeChange}
                                options={this.state.selectStart}
                            />
                            <span className="help-block" style={{color:'red'}}>{validation.startTime.message}</span>
                        </div>
                    </div>}

                    {this.state.startTimeSelected === true && <div className= {validation.endTime.isInvalid ? 'has-error': undefined}>
                        <div className={'selectStyle2'}><label>End time:</label>
                            <Select
                                value={this.state.endTime}
                                onChange={this.handleEndTimeChange}
                                options={this.state.selectEnd}
                            />
                            <span className="help-block" style={{color:'red'}}>{validation.endTime.message}</span>
                        </div>
                    </div>}
                    <br style={{ clear:"left"}}/>

                    <button type="Submit" className="btn btn-primary btn-block buttonPrice" style={{marginLeft:"78%"}} onClick={this.handleSubmit}>See price <i className="fa fa-angle-double-right"/></button>

                </form>

            </div>
        )
    };

}export default withRouter(MakeSubscription)