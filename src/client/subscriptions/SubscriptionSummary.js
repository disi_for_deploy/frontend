import React, {Component} from "react";
import {withRouter} from "react-router";
import ManageCourtsService from "../../services/ManageCourtsService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {Button, DialogActions} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import moment from 'moment';
import SubscriptionService from "../../services/SubscriptionService";
import './SubcriptionSummary.css';

class SubscriptionSummary extends Component{

    constructor(props) {
        super(props);
        this.state = {
            totalPrice:'',
            subscription: JSON.parse(localStorage.getItem("subscription")),
            client: JSON.parse(sessionStorage.getItem("client")),
            courtNumber: '',
            location: '',
            openDialogPay: false,
            okSubscription: true
        }
    }

    componentDidMount(){
        SubscriptionService.computePrice(this.state.subscription).then(response =>{
            this.setState({totalPrice:response.data}, function () {
                console.log(this.state.totalPrice)
            })
        })

        ManageCourtsService.getCourtById(this.state.subscription.courtId).then(response =>{
            this.setState({courtNumber:response.data.courtNumber})
            this.setState({location:response.data.locationAddress})
        })
    }

    verifySubscription(){
        let ok = 1;
        SubscriptionService.getSubscriptions().then(response => {
            response.data.map(subscription =>{
                if(subscription.username === this.state.client.username){
                    if(moment(subscription.startDate).subtract(1,'month').add(subscription.endTime, 'hours').toISOString() > moment().toISOString()){
                        this.setState({okSubscription:false});
                        ok = 0;
                    }
                }
                return subscription
            })

            if(ok === 1){
                SubscriptionService.postSubscription(this.state.subscription).then(response =>{
                    console.log(response.data)
                })
            }
        });
    }

    handleCancel = (event) => {
        event.preventDefault();
        localStorage.removeItem("subscription");
        this.props.history.push('/');
    }

    handlePay = (event) => {
        event.preventDefault();
        console.log(this.state.subscription)
        this.verifySubscription();
        this.setState({openDialogPay: true})
    }

    handleToClose = () => {
        localStorage.removeItem("subscription");
        this.setState({openDialogPay: false});
        this.props.history.push('/mySubscriptions/' + this.state.client.username);
        window.location.reload(false);
    }

    render() {
        return(
            <div className={'SubscriptionSummary'}>
                <form>
                    <fieldset>
                        <h4 className={'text1'}>Subscription Summary</h4><br/>
                        <p className={'info'}><i className="fa fa-user"/> Name: <b>{this.state.client.firstName+" "+this.state.client.lastName}</b></p>
                        <p className={'info'}><i className="fa fa-th"/> Court number: <b>{this.state.courtNumber}</b></p>
                        <p className={'info'}><i className="fa fa-map-marker"/> Location address: <b>{this.state.location}</b></p>
                        <p className={'info'}><i className="fa fa-calendar-check-o"/> Period: <b>{moment(this.state.subscription.startDate).format('DD/MM/YYYY')} - {moment(this.state.dateSubscription).add(1,'month').subtract(1,'day').format('DD/MM/YYYY')}</b></p>
                        <p className={'info'}><i className="fa fa-clock-o"/> Time: <b>{this.state.subscription.startTime+":00"} - {this.state.subscription.endTime+":00"}</b></p>
                        <p className={'info'}><i className="fa fa-dollar" style={{marginLeft:'1%'}}/> Total price: <b>{this.state.totalPrice}</b></p>
                    </fieldset><br/>

                    <button className="btn btn-danger" style={{ marginLeft: "1%", marginRight: "35%" }} onClick={this.handleCancel}>CANCEL</button>
                    <button className="btn btn-success" style={{marginLeft: "10%"}}>Pay with crypto</button>
                    <button className="btn btn-success" style={{marginLeft: "1%"}} onClick={this.handlePay}>Pay</button>

                    <Dialog open={this.state.openDialogPay}>
                        <DialogContent>
                            <DialogContentText>
                                {this.state.totalPrice > this.state.client.wallet &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                    <span>You don't have enough money to make the subscription.</span></div>}
                                {this.state.totalPrice <= this.state.client.wallet && this.state.okSubscription === true &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                    <span>Your subscription was made with success.</span></div>}
                                {this.state.totalPrice <= this.state.client.wallet && this.state.okSubscription === false &&
                                <div><span style={{fontSize:'22px'}}>{this.state.client.firstName+","}</span><br/>
                                    <span>You can not have more than one subscription in the future and you already have one.</span></div>}

                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleToClose}
                                    color="primary" autoFocus>
                                Close
                            </Button>
                        </DialogActions>
                    </Dialog>

                </form>
            </div>
        )
    }

}export default withRouter(SubscriptionSummary)