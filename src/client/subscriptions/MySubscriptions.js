import React,{Component} from "react";
import {withRouter} from "react-router";
import './MySubscriptions.css'
import SubscriptionService from "../../services/SubscriptionService";
import moment from "moment";
import ManageCourtsService from "../../services/ManageCourtsService";

class MySubscriptions extends Component{

    constructor(props) {
        super(props)

        this.state = {
            subscriptions: [],
            client: JSON.parse(sessionStorage.getItem("client")),
            locationAddress: '',
            courtNb: '',
            username: this.props.match.params.username,
            dateNow: moment().toISOString(),
            courts: []
        }
    }

    componentDidMount(){
        SubscriptionService.getSubscriptions().then(response =>{
            this.setState({ subscriptions: response.data })
        })

        ManageCourtsService.getAllCourts().then(response =>{
            this.setState({ courts: response.data })
        });
    }

    getNumber(id) {
        let nb;
        this.state.courts.map(court => {
            if(court.idCourt === id) { nb = court.courtNumber}
            return nb
        })
        return nb
    }

    getLocation(id) {
        let location;
        this.state.courts.map(court => {
            if(court.idCourt === id) { location = court.locationAddress}
            return location
        })
        return location
    }

    renderTableHeaderSubscriptions() {
        let headerElement = ['Start date', 'End date', 'Start time', 'End time', 'Day of week', 'Price']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableHeaderSubscriptionsHistory() {
        let headerElement = ['Start date', 'End date', 'Start time', 'End time', 'Location', 'Court number', 'Price']

        return headerElement.map((key, index) => {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableDataSubscriptions() {
        return this.state.subscriptions.map(subscription => {
            const { idSubscription, startDate, startTime, endTime, totalPrice, dayOfWeek, username } = subscription
            return this.state.dateNow <= moment(startDate).add(endTime, 'H').toISOString()
            && this.state.username === username
                ? (
                <tr key={idSubscription}>
                    <td>{moment(startDate).subtract(1, 'M').format('DD-MM-YYYY')}</td>
                    <td>{moment(startDate).subtract(1,'day').format('DD-MM-YYYY')}</td>
                    <td>{startTime}</td>
                    <td>{endTime}</td>
                    <td>{dayOfWeek}</td>
                    <td>{totalPrice}</td>
                </tr>
            ): undefined
        })

    }

    renderTableDataSubscriptionsHistory() {
        return this.state.subscriptions.map(subscription => {
            const { idSubscription, startDate, startTime, endTime, totalPrice, courtId, username } = subscription
            return this.state.dateNow > moment(startDate).add(endTime, 'H').toISOString()
            && this.state.username === username
                ? (
                <tr key={idSubscription}>
                    <td >{moment(startDate).subtract(1, 'M').format('DD-MM-YYYY')}</td>
                    <td>{moment(startDate).subtract(1,'day').format('DD-MM-YYYY')}</td>
                    <td>{startTime}</td>
                    <td>{endTime}</td>
                    <td>{this.getLocation(courtId)}</td>
                    <td>{this.getNumber(courtId)}</td>
                    <td>{totalPrice}</td>
                </tr>
            ): undefined
        })

    }

    render() {
        return(
            <div className={'MySubscriptions'}>
                <form>
                    <h4 align={'center'} style={{ padding:'2%' }}>My subscriptions</h4>
                    <h6 style={{ marginLeft:'11%' }}>ON GOING</h6>
                    <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                        <tbody>
                        <tr>{this.renderTableHeaderSubscriptions()}</tr>
                        {this.renderTableDataSubscriptions()}
                        </tbody>
                    </table><br/>

                    <h6 style={{ marginLeft:'11%' }}>HISTORY</h6>
                    <table id='locations' style={{ width: '80%', marginLeft: '10%', marginTop: '3%' }}>
                        <tbody>
                        <tr>{this.renderTableHeaderSubscriptionsHistory()}</tr>
                        {this.renderTableDataSubscriptionsHistory()}
                        <br/>
                        </tbody>
                    </table>
                </form>
            </div>
        )
    }

}export default withRouter(MySubscriptions)