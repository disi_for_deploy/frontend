import React, {Component} from 'react';
import {withRouter} from "react-router";
import './Wallet.css'
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"
import moment from 'moment';
import ClientService from "../../services/ClientService";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {Button, DialogActions} from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";

class Wallet extends Component{
    constructor(props) {
        super(props);

        this.validator1 = new RegisterLoginValidator([{
            field: 'money_field',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter money.'
        },{
            field: 'money_field',
            method: this.isNumber,
            validWhen: true,
            message: '* Enter a valid number.'
        }]);

        this.validator2 = new RegisterLoginValidator([{
            field: 'crypto_field',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter crypto.'
        },{
            field: 'crypto_field',
            method: this.isNumber,
            validWhen: true,
            message: '* Enter a valid number.'
        }]);

        this.state={
            dateToday: moment().format('MMMM Do YYYY'),
            client: JSON.parse(sessionStorage.getItem("client")),
            money: 0,
            crypto: 0,
            money_field: '',
            crypto_field: '',
            username: this.props.match.params.username,
            validation1: this.validator1.valid(),
            validation2: this.validator2.valid(),
            addMoney: false,
            openDialogMoney: false,
            openDialogCrypto: false
        }

        this.submitted = false;
        this.state.money = this.state.client.wallet;
        this.state.crypto = this.state.client.cryptoWallet;
    }

    isNumber(myString) { return /^[0-9]+$/.test(myString)}

    backFunction = () =>{
        this.props.history.push('/');
    }

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleAddMoney = event => {
        event.preventDefault();
        const validation1 = this.validator1.validate(this.state);
        this.setState({
            validation1
        });
        this.setState({addMoney:true})
        this.submitted = true;
        if(validation1.isValid) {
            console.log('ok');
            let money=this.state.money + parseInt(this.state.money_field, 10);
            this.setState({money:money}, function () {
                this.updateClient();
            });
            this.setState({openDialogMoney:true});
        } else console.log('not ok');
    }

    handleAddCrypto = event => {
        event.preventDefault();
        const validation2 = this.validator2.validate(this.state);
        this.setState({
            validation2
        });
        this.submitted = true;
        this.setState({addMoney:false})
        if(validation2.isValid) {
            console.log('ok');
            let crypto=this.state.crypto + parseInt(this.state.crypto_field, 10);
            this.setState({crypto:crypto}, function (){
                this.updateClient();
            });
            this.setState({openDialogCrypto:true});
        } else console.log('not ok');
    }

    updateClient(){
        let client = {
            id: this.state.client.idClient,
            firstName: this.state.client.firstName,
            lastName: this.state.client.lastName,
            address: this.state.client.address,
            birthday: this.state.client.birthday,
            wallet: this.state.money,
            cryptoWallet: this.state.crypto,
            username: this.state.username
        };
        console.log('client => ' + JSON.stringify(client));

        ClientService.updateClient(client, this.state.client.idClient).then(response =>{
            console.log('client => ' + JSON.stringify(response));
        })
    }

    handleToClose = () => {
        this.setState({openDialogMoney:false});
        this.setState({openDialogCrypto:false});
        this.props.history.push('/');
        window.location.reload(false);
    }

    render() {

        let validation1 = this.submitted ?this.validator1.validate(this.state) : this.state.validation1;
        let validation2 = this.submitted ?this.validator2.validate(this.state) : this.state.validation2
        return(
            <div className={'Wallet'}>
                <form>
                    <h4 align={'center'} style={{ padding:'3%' }}>My wallet</h4>
                    <p align={'center'}>On {this.state.dateToday}, you still have ${this.state.money} and {this.state.crypto} cryptocurrencies</p>

                    <div className= {validation1.money_field.isInvalid ? 'has-error': undefined}>
                    <input className="form-control input1" name={"money_field"} onChange={this.handleInputChange}  placeholder={'      Insert $'}>
                    </input>
                     </div>

                    <div className= {validation2.crypto_field.isInvalid ? 'has-error': undefined}>
                    <input className="form-control input2" name={"crypto_field"} onChange={this.handleInputChange} placeholder={'   Insert crypto'}>
                    </input>
                    </div>

                    {this.state.addMoney===true && <span className="help-block" style={{color:'red', marginLeft:'13%'}}>{validation1.money_field.message}</span>}
                    {this.state.addMoney===false && <span className="help-block" style={{color:'red', marginLeft:'61%'}}>{validation2.crypto_field.message}</span>}

                    <br/>
                    <button className="btn btn-primary btn-block buttonMoney" onClick={this.handleAddMoney}>Add money</button>
                    <button className="btn btn-primary btn-block buttonCrypto" onClick={this.handleAddCrypto}>Add crypto</button>
                    <button className="btn btn-primary btn-block buttonBack" onClick={this.backFunction}>BACK</button>
                    <p></p>

                    <Dialog open={this.state.openDialogMoney}>
                        <DialogContent>
                            <DialogContentText>
                                The money was added to wallet.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleToClose}
                                    color="primary" autoFocus>
                                Close
                            </Button>
                        </DialogActions>
                    </Dialog>

                    <Dialog open={this.state.openDialogCrypto}>
                            <DialogContent>
                                <DialogContentText>
                                    The crypto was added to crypto wallet.
                                </DialogContentText>
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={this.handleToClose}
                                        color="primary" autoFocus>
                                    Close
                                </Button>
                            </DialogActions>
                        </Dialog>
                </form>

            </div>
        )
    }

}export default withRouter(Wallet)