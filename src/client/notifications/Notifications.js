import React, { Component } from "react";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class Notifications extends Component {

    constructor(props) {
        super(props)

        this.state = {
            notifications: []
        }

        this.notificationDetails = this.notificationDetails.bind(this);
    }

    componentDidMount() {
         if(cookies.get("Transfer_partner")) {
            const newSelect = this.state.notifications;
            const element = cookies.get("Transfer_partner");
            if (JSON.parse(sessionStorage.getItem("client")).username !== element.username) {
                newSelect.push({title: element.title, description: element.description, id: element.reservationId});
                this.setState({notifications: newSelect});
            }
        }

        if(cookies.get("Request_partner")) {
            const newSelect = this.state.notifications;
            const element = cookies.get("Request_partner");
            newSelect.push({title: element.title, description: element.description, id: element.reservationId});
            this.setState({notifications: newSelect});
        }
    }

    notificationDetails = event => {
        this.props.history.push("myReservation/viewReservation/" + event.target.id)
    }

    render() {

        return (
            <div>
                <h1 style={{
                    marginTop: "3%",
                    fontWeight: "bold",
                    color: "white",
                    textAlign: "center",
                    textShadow: "2px 2px #5D6799"
                }}>My Notifications</h1>
                {(this.state.notifications.length > 0 || this.state.notifications.length !== undefined) &&
                    <>
                        {this.state.notifications.map((notification, i) =>
                            <div key={i} className="card" style={{width: "30%", marginLeft: "35%", marginTop: "3%", padding: "10px"}}>
                                <div className="card-body">
                                    <h5 className="card-title">{notification.title}</h5>
                                    <p className="card-text">{notification.description}</p>
                                    <button type="button" id={notification.id} onClick={this.notificationDetails}
                                            className="btn btn-warning">Details
                                    </button>
                                </div>
                            </div>
                        )}
                    </>
                }
            </div>
        );
    }
}
