import React, { Component } from "react";
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/bootstrap/dist/js/bootstrap.min.js';
import 'font-awesome/css/font-awesome.css';
import './JoinReservation.css';
import swal from 'sweetalert';
import ReservationService from "../../services/ReservationService";

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    marginLeft: '20%',
    width: '60%'
};


export default class JoinReservation extends Component {

    constructor(props) {
        super(props)

        this.state = {
            code: '',
            username: this.props.match.params.username,
            number: '',
            code2: ''
        }

        this.sendCode = this.sendCode.bind(this);
        this.sendCode2 = this.sendCode2.bind(this);
    }


    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    sendCode() {

        var num = localStorage.getItem('reservation_code').match(/\d+/)[0];
        if (num < 10) {
            ++num;
            let username = localStorage.getItem('reservation_code').substring(1, num);
            let code = localStorage.getItem('reservation_code').substring(num, localStorage.getItem('reservation_code').length);
            if (code === this.state.code) {
                localStorage.setItem('request_username', username);
                localStorage.setItem('accepted', true);
                localStorage.setItem('requested_partner', this.state.username);
                console.log("username= ", username)
                console.log("code= ", code)
                swal("Success", "Request sent successfully! ", "success");
            } else {
                swal("Error", "Code do not match", "error");
            }
        } else {
            ++num;
            ++num;

            let username = localStorage.getItem('reservation_code').substring(2, num);
            let code = localStorage.getItem('reservation_code').substring(num, localStorage.getItem('reservation_code').length);
            if (code === this.state.code) {
                localStorage.setItem('partner_username', username);
                localStorage.setItem('accepted_partner', true);
                console.log("username= ", username)
                console.log("code= ", code)
                swal("Success", "Request sent successfully! ", "success");
            } else {
                swal("Error", "Code do not match", "error");
            }
        }
    }

    sendCode2() {
        console.log(this.state.code2);
        console.log(localStorage.getItem('transferCode'));
        if (this.state.code2 === localStorage.getItem('transferCode')) {
            let c = localStorage.getItem('contor');
            console.log(c)
            c++;
            console.log(c)
            localStorage.setItem('contor', c);
            swal("Success", "Request sent successfully! ", "success");
            if (localStorage.getItem('contor') >= 2) {
                let data = {
                    "idReservation": localStorage.getItem('idReservation'),
                    "username": JSON.parse(sessionStorage.getItem("client")).username,
                    "courtId": localStorage.getItem('courtId')
                }
                ReservationService.changeClient(data).then(response => {
                    console.log(response)
                })
            }
        }
        else{
            swal("Error", "Code do not match", "error");
        }
    }

    render() {

        return (
            <div style={{ backgroundColor: "white", borderRadius: "2%", width: "30%", height: "450%", marginLeft: "35%", marginTop: "10%", opacity: "0.8" }}>

                <form >
                    <h4 align={'center'} style={{ padding: '2%' }}>Join Reservation</h4>

                    <b style={{ marginLeft: '32%' }}>Enter code from mail: </b>
                    <input style={inputStyle} type="string" className="form-control" name="code" onChange={this.handleInputChange} />
                </form>
                <button style={{ marginLeft: "40%", marginTop: "3%", marginBottom: "5%" }} onClick={this.sendCode} type="button" className="btn btn-secondary">Submit</button>

                <form >
                    <h4 align={'center'} style={{ padding: '2%' }}>Take Reservation</h4>

                    <b style={{ marginLeft: '23%' }}>Enter code from notification: </b>
                    <input style={inputStyle} type="string" className="form-control" name="code2" onChange={this.handleInputChange} />
                </form>
                <button style={{ marginLeft: "40%", marginTop: "3%", marginBottom: "5%" }} onClick={this.sendCode2} type="button" className="btn btn-secondary">Submit</button>

            </div>
        );
    }
}
