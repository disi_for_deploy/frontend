import React, { Component } from "react";
import './Register.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import moment from 'moment';
import LoginAndRegistrationService from "../../services/LoginAndRegistrationService";
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

class Register extends Component {

    constructor(props) {
        super(props);
        this.validator = new RegisterLoginValidator([{
            field: 'first_name',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter first name.'
        }, {
            field: 'first_name',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* First name should not have spaces.'
        },{
            field: 'first_name',
            method: this.startsWithCapital,
            validWhen: true,
            message: '* First name should start with a capital letter.'
        },  {
            field: 'first_name',
            method: this.onlyLetters,
            validWhen: true,
            message: '* First name should not contain numbers or special characters, except -.'
        }, {
            field: 'first_name',
            method: this.minLengthName,
            validWhen: true,
            message: '* First name should have at least 3 characters.'
        }, {
            field: 'last_name',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* Last name should not have spaces.'
        }, {
            field: 'last_name',
            method: this.startsWithCapital,
            validWhen: true,
            message: '* Last name should start with a capital letter.'
        }, {
            field: 'last_name',
            method: this.onlyLetters,
            validWhen: true,
            message: '* Last name should not contain any number .'
        }, {
            field: 'last_name',
            method: this.minLengthName,
            validWhen: true,
            message: '* Last name should have at least 3 characters.'
        }, {
            field: 'last_name',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter last name.'
        }, {
            field: 'username',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter username.'
        }, {
            field: 'username',
            method: this.startsWithLetter,
            validWhen: true,
            message: '* Username should start with a  letter.'
        }, {
            field: 'username',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* Username should not have spaces.'
        }, {
            field: 'username',
            method: this.minLength,
            validWhen: true,
            message: '* Username should have at least 5 characters.'
        }, {
            field: 'username',
            method: this.maxLength,
            validWhen: true,
            message: '* Username should have maximum 20 characters.'
        }, {
            field: 'email',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter your email address.'
        }, {
            field: 'address',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter address.'
        }, {
            field: 'address',
            method: this.onlySpaces,
            validWhen: false,
            message: '* Address is empty.'
        }, {
            field: 'address',
            method: this.addressFormat,
            validWhen: true,
            message: '* Address should have a valid format.'
        }, {
            field: 'birthday',
            method: 'isEmpty',
            validWhen: false,
            message: '* Select birthday.'
        }, {
            field: 'birthday',
            method: this.birthDayCheck,
            validWhen: true,
            message: '* You should be at leat 18 years old to register.'
        }, {
            field: 'email',
            method: 'isEmail',
            validWhen: true,
            message: '* Invalid email address.'
        }, {
            field: 'password',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* Password should not have spaces.'
        }, {
            field: 'password',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter password.'
        }, {
            field: 'password',
            method: this.passCharacters,
            validWhen: true,
            message: '* Password should have at least 1 number, 1 capital letter and 1 special character.'
        }, {
            field: 'password',
            method: this.minLength,
            validWhen: true,
            message: '* Password should have at least 5 characters.'
        }, {
            field: 'password_confirmation',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter password confirmation.'
        }, {
            field: 'password_confirmation',
            method: this.passwordMatch,
            validWhen: true,
            message: '* Password and password confirmation do not match.'
        }]);

        this.state = {
            first_name: '',
            last_name: '',
            address: '',
            birthday: '',
            email: '',
            password: '',
            password_confirmation: '',
            validation: this.validator.valid(),
            maxDate: null,
        }
        this.submitted = false;
        this.state.maxDate = moment().subtract(18, "years").format('YYYY-MM-DD');

    }

    passwordMatch = (confirmation, state) => (state.password === confirmation)

    minLength(val) { return (val.length > 4)}

    maxLength(val) { return (val.length < 21)}

    containsWhitespace(myString) { return /\s/.test(myString)}

    onlySpaces(myString) { return /^\s*$/.test(myString)}

    minLengthName(val) { return (val.length > 2)}

    onlyLetters(myString) { return /^[a-zA-Z-]+$/.test(myString)}

    startsWithCapital(myString){ return /[A-Z]/.test(myString.charAt(0))}

    startsWithLetter(myString){ return /[a-zA-Z]/.test(myString.charAt(0))}

    passCharacters(myString) { return /[A-Z]/.test(myString)
        && /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>?~]/.test(myString)
        && /\d/.test(myString) }

    addressFormat(myString) { return  /[a-zA-Z0-9\s]{3,}(.)? (\d+)/.test(myString)}

    birthDayCheck(date) {return date <= moment().subtract(18, "years").format('YYYY-MM-DD')}

    handleInputChange = event => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    registerClient(){
        let client = {
            username: this.state.username,
            password: this.state.password,
            role: "client",
            email: this.state.email,
            firstName: this.state.first_name,
            lastName: this.state.last_name,
            address: this.state.address,
            birthday: moment(this.state.birthday).toISOString()
        };
        console.log('client => ' + JSON.stringify(client));

        LoginAndRegistrationService.registerUser(client).then(response =>{
            console.log("ok");
            this.props.history.push("/login");
        });
    }
    handleFormSubmit = event => {
        event.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;
        if(validation.isValid) {
            this.registerClient();
        } else console.log('not ok');

    }

    MouseOver(e) {
        e.target.style.color = 'green';
    }

    MouseOut(event){
        event.target.style.color="yellow";
    }

    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return (

            <div className="Register">
                <form>
                    <h4 align={'center'}>Register </h4>
                    <div className= {validation.first_name.isInvalid ? 'has-error': undefined}>
                        <label>First name</label>
                        <input style={inputStyle} type="string" className="form-control" name="first_name" placeholder="First name" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.first_name.message}</span>
                    </div>

                    <div className={validation.last_name.isInvalid ? 'has-error': undefined}>
                        <label>Last name</label>
                        <input style={inputStyle} type="string" className="form-control" name="last_name"  placeholder="Last name" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.last_name.message}</span>
                    </div>

                    <div className={validation.address.isInvalid ? 'has-error': undefined}>
                        <label>Address</label>
                        <input style={inputStyle} type="string" className="form-control" name="address" placeholder="Address" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.address.message}</span>
                    </div>

                    <div className={validation.birthday.isInvalid ? 'has-error': undefined}>
                        <label>Birthday</label>
                        <input style={inputStyle} type="date" max={this.state.maxDate} className="form-control" name="birthday" placeholder="Birthday" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.birthday.message}</span>
                    </div>

                    <div className={validation.username.isInvalid ? 'has-error': undefined}>
                        <label>Username</label>
                        <input style={inputStyle} type="string" className="form-control" name="username" placeholder="Enter username" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.username.message}</span>
                    </div>

                    <div className={validation.email.isInvalid ? 'has-error': undefined}>
                        <label>Email address</label>
                        <input style={inputStyle} type="email" className="form-control" name="email" placeholder="Enter email" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.email.message}</span>
                    </div>

                    <div className={validation.password.isInvalid ? 'has-error': undefined}>
                        <label>Password</label>
                        <input style={inputStyle} type="password" className="form-control" name="password" placeholder="Enter password" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.password.message}</span>
                    </div>

                    <div className={validation.password_confirmation.isInvalid ? 'has-error': undefined}>
                        <label>Confirm password</label>
                        <input style={inputStyle} type="password" className="form-control" name="password_confirmation" placeholder="Confirm password" onChange={this.handleInputChange} />
                        <span className="help-block" style={{color:'red'}}>{validation.password_confirmation.message}</span>
                    </div>
                    <p/>

                </form>

                <button onClick={this.handleFormSubmit} type="submit" className="btn btn-primary btn-block">Register</button>
                <p style={{color:"white", marginLeft: "64%", marginTop: "2%"}} className="forgot-password text-right">
                    Already registered? <a style={{ textDecoration: 'none', color:"yellow"}} onMouseOver={this.MouseOver} onMouseOut={this.MouseOut} href="/login">Login</a>
                </p>

            </div>
        );
    }
}
export default Register;