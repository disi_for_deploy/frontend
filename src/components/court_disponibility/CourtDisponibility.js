import React, { Component } from "react";
import './CourtDisponibility.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import moment from 'moment';
import ReservationService from "../../services/ReservationService"

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white",
    marginTop: "5%"
};

export default class CourtDisponibility extends Component {

    constructor(props) {
        super(props)

        this.state = {
            intervals: '',
            id: this.props.match.params.id,
            minDate: null,
            intervalsPrim: []
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.state.minDate = moment().add(1, 'days').format('YYYY-MM-DD');
    }

    handleInputChange = event => {
        this.setState({ intervalsPrim: [] })
        let data = {
            date: event.target.value,
            courtId: this.state.id
        }
        ReservationService.getAvailableIntervals(data).then(response => {
            this.setState({ intervals: response.data })
            Object.keys(response.data).forEach(element => {
                //  this.state.intervalsPrim.push({ key: element, array: response.data[element] })
                const newSelect = this.state.intervalsPrim;
                newSelect.push({ key: element, e1: response.data[element][0] ,e2: response.data[element][1] });
                this.setState({ intervalsPrim: newSelect });
            })
        });
    }

    render() {

        return (
            <div className="container">
                <h1 align={'center'} style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Court availability</h1>
                <input style={inputStyle} type="date" className="form-control" min={this.state.minDate} name="birthday" onChange={this.handleInputChange} placeholder="Verify disponibility" />
                <div className="row">
                    {Object.keys(this.state.intervals).forEach(element =>
                        <div className="col-md-4 col-sm-6 col-xs-12">
                            <button className="button-53">{this.state.intervals[element]}</button>
                        </div>
                    )}

                    {this.state.intervalsPrim.map((element, index)=>

                        <div key={index} className="col-md-4 col-sm-6 col-xs-12" style={{marginTop:"2%"}}>
                            {element.e2 === undefined ?
                                <button className="button-53">{element.key}-{element.e1}</button> : element.e1 === undefined ?  '': <button className="button-53">{element.key}-{element.e1}, {element.key}-{element.e2}</button>}
                        </div>
                    )}

                </div>
            </div>
        );
    }
}