import React, { Component } from "react";
import './Login.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import LoginAndRegistrationService from "../../services/LoginAndRegistrationService"
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"
import jwt from 'jwt-decode'
import swal from 'sweetalert'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

export default class Login extends Component {

    constructor(props) {
        super(props)

        this.changeUsernameHandler = this.changeUsernameHandler.bind(this);
        this.changePasswordHandler = this.changePasswordHandler.bind(this);
        this.login = this.login.bind(this);

        this.validator = new RegisterLoginValidator([{
            field: 'username',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter username.'
        }, {
            field: 'password',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter password.'
        }]);


        this.state = {
            username: '',
            password: '',
            validation: this.validator.valid(),
        }
    }

    MouseOver(e) {
        e.target.style.color = '#00FF00';
    }

    MouseOut(event) {
        event.target.style.color = "yellow";
    }

    login = (e) => {
        e.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;

        if (validation.isValid) {
            console.log('ok');
            const user = {username: this.state.username, password: this.state.password};

            LoginAndRegistrationService.login(user).then(response => {
                console.log("user => ", JSON.stringify(response.data))

                const token = response.data.access_token;
                const userRole = jwt(token); // decode token here

                if (userRole.roles[0] === 'client') {
                    localStorage.setItem("login_token", token)

                    //partea de partner
                    if (localStorage.getItem('request_username') === this.state.username &&
                        localStorage.getItem('accepted') === 'true') {
                        localStorage.removeItem('reservation_code');
                        localStorage.removeItem('request_username');
                        localStorage.removeItem('accepted');

                        let data = {
                            title: "Request accepted",
                            description: "Your request for a partner was accepted by: " + localStorage.getItem('requested_partner'),
                            reservationId: localStorage.getItem("reservation_id")
                        }

                        cookies.set("Request_partner", data);
                    }
                    // end partner
                    this.props.history.push('/');
                    window.location.reload(false)
                } else if (userRole.roles[0] === 'admin') {
                    localStorage.setItem("login_token", token)
                    this.props.history.push('/adminHomePage');
                    window.location.reload(false)
                } else {
                    console.log('Incorrect username or password => ' + JSON.stringify(response.data));
                    swal("Warning", "Incorrect username or password", "warning");
                    this.props.history.push('/login');
                }
            });
        } else {
            console.log('not ok');
        }
    }

    changeUsernameHandler = (event) => {
        this.setState({ username: event.target.value });
    }

    changePasswordHandler = (event) => {
        this.setState({ password: event.target.value });
    }

    render() {

        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation
        return (
            <div>
                <div className="Login">
                    <form>
                        <h4 align={'center'}>Sign up </h4>

                        <div className={validation.username.isInvalid ? 'has-error' : undefined}>
                            <label>Username</label>
                            <input style={inputStyle} type="text" className="form-control" placeholder="Enter username" value={this.state.username} onChange={this.changeUsernameHandler} required />
                            <span className="help-block" style={{ color: 'red' }}>{validation.username.message}</span>
                        </div>

                        <div className={validation.password.isInvalid ? 'has-error' : undefined}>
                            <label>Password</label>
                            <input style={inputStyle} type="password" className="form-control" placeholder="Enter password" value={this.state.password} onChange={this.changePasswordHandler} required />
                            <span className="help-block" style={{ color: 'red' }}>{validation.password.message}</span>
                        </div>
                    </form>

                    <button type="submit" className="btn btn-primary btn-block" onClick={this.login}>Login</button>
                    <p align={'center'} style={{ color: "white", marginLeft: "37%", marginTop: "3%" }} className="ex1">
                        <a onMouseOver={this.MouseOut} onMouseOut={this.MouseOver} style={{ textDecoration: 'none', color: "#00FF00" }} href="/resetPasswordEmail"> Forgot your password? </a>
                    </p>

                    <p align={'center'} style={{ color: "white", marginLeft: "37%" }} className="forgot-password text-right">
                        Don't have an account? <a onMouseOver={this.MouseOver} onMouseOut={this.MouseOut} style={{ textDecoration: 'none', color: "yellow" }} href="/register">Register</a>
                    </p>

                </div>
            </div>
        );
    }
}
