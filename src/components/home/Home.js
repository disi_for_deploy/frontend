import React from 'react';
import sample from '../../commons/videos/tennis_video.mp4';
import './Home.css'

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            message: "Your server message here."
        }
    }

    render() {

        return (

            <div className="video-banner">
                <video className='videoTag' autoPlay loop muted style={{ width: '100%' }}>
                    <source src={sample} type='video/mp4' />
                </video>
                <div className="overlay">
                    <div className="threeD">
                        TENNIS CLUB
                    </div>

                </div>

            </div>
        )
    };
}

export default Home