import React, { Component } from "react";
import './ResetPassword.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"
import ResetPasswordService from "../../services/ResetPasswordService"
let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

export default class ResetPasswordCode extends Component {

    constructor(props) {
        super(props)

        this.changeCodeHandler = this.changeCodeHandler.bind(this);
        this.changePassword1Handler = this.changePassword1Handler.bind(this);
        this.changePassword2Handler = this.changePassword2Handler.bind(this);
        this.resetPassword = this.resetPassword.bind(this);

        this.validator = new RegisterLoginValidator([{
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter code.'
        }, {
            field: 'password1',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter password.'
        },  {
            field: 'password1',
            method: this.containsWhitespace,
            validWhen: false,
            message: '* Password should not have spaces.'
        }, {
            field: 'password1',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter password.'
        }, {
            field: 'password1',
            method: this.passCharacters,
            validWhen: true,
            message: '* Password should have at least 1 number, 1 capital letter and 1 special character.'
        }, {
            field: 'password1',
            method: this.minLength,
            validWhen: true,
            message: '* Password should have at least 5 characters.'
        }, {
            field: 'password2',
            method: 'isEmpty',
            validWhen: false,
            message: '* Re-Enter password.'
        }, {
            field: 'password2',
            method: this.passwordMatch,
            validWhen: true,
            message: '* Password and password confirmation do not match.'
        },  {
            field: 'code',
            method: this.codeMatch,
            validWhen: true,
            message: '* Code do not match.'
        }]);

        
        this.state = {
            email: this.props.match.params.email,
            code: '',
            confirmationCode:  localStorage.getItem("reset_code") ,
            password1: '',
            password2: '',
            validation: this.validator.valid(),
        }
    }

    passCharacters(myString) { return /[A-Z]/.test(myString)
        && /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>?~]/.test(myString)
        && /\d/.test(myString) }

    minLength(val) { return (val.length > 4)}

    containsWhitespace(myString) { return /\s/.test(myString)}

    passwordMatch = (password2, state) => (state.password1 === password2)

    codeMatch = (code, state) => (state.confirmationCode === code)

    resetPassword = (e) => {
        e.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;
        if(validation.isValid) {
            console.log('ok');
        } else console.log('not ok');

         e.preventDefault();
         let resetPassword = {password: this.state.password1, confirmationPassword: this.state.password2};
         console.log('reset password => ' + JSON.stringify(resetPassword));

         ResetPasswordService.sendResetPassword(resetPassword, this.state.email).then(response =>{
             console.log('user => ' + JSON.stringify(response));
             localStorage.removeItem('reset_code');

             this.props.history.push('/login');
         });
    }

    changeCodeHandler= (event) => {
        this.setState({code: event.target.value});
    }

    changePassword1Handler= (event) => {
        this.setState({password1: event.target.value});
    }

    changePassword2Handler= (event) => {
        this.setState({password2: event.target.value});
    }


    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return (
            <div>
                <div className="ResetPassword">
                    <form>
                        <h4 align={'center'}>Reset your password:</h4>

                        <div className={validation.code.isInvalid ? 'has-error' : undefined}>
                            <label>Code</label>
                            <input style={inputStyle} type="text" className="form-control" placeholder="Enter code from email" value={this.state.code} onChange={this.changeCodeHandler} required />
                            <span className="help-block" style={{color:'red'}}>{validation.code.message}</span>
                        </div>

                        <div className={validation.password1.isInvalid ? 'has-error' : undefined}>
                            <label>Password</label>
                            <input style={inputStyle} type="password" className="form-control" placeholder="Enter password" value={this.state.password1} onChange={this.changePassword1Handler} required />
                            <span className="help-block" style={{color:'red'}}>{validation.password1.message}</span>
                        </div>

                        <div className={validation.password2.isInvalid ? 'has-error' : undefined}>
                            <label>Re-Enter Password</label>
                            <input style={inputStyle} type="password" className="form-control" placeholder="Re-Enter password" value={this.state.password2} onChange={this.changePassword2Handler} required />
                            <span className="help-block" style={{color:'red'}}>{validation.password2.message}</span>
                        </div>
                    </form>

                    <button type="submit" className="btn btn-primary btn-block" onClick={this.resetPassword}>Reset</button>

                </div>
            </div>
        );
    }
}
