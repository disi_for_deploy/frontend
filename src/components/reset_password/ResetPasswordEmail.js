import React, { Component } from "react";
import './ResetPassword.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import ResetPasswordService from "../../services/ResetPasswordService"
import RegisterLoginValidator from "../../services/validators/Register&LoginValidator"

let inputStyle;
inputStyle = {
    backgroundColor: "transparent",
    color: "white"
};

export default class ResetPasswordEmail extends Component {

    constructor(props) {
        super(props)

        this.changeEmailHandler = this.changeEmailHandler.bind(this);
        this.sendCode = this.sendCode.bind(this);

        this.validator = new RegisterLoginValidator([{
            field: 'email',
            method: 'isEmpty',
            validWhen: false,
            message: '* Enter email.'
        }, {
            field: 'email',
            method: 'isEmail',
            validWhen: true,
            message: '* Invalid email address.'
        }]);

        
        this.state = {
            email: '',
            validation: this.validator.valid(),
        }
    }

    sendCode = (e) => {
        e.preventDefault();
        const validation = this.validator.validate(this.state);
        this.setState({
            validation
        });
        this.submitted = true;
        if(validation.isValid) {
            ResetPasswordService.sendEmailForCode(this.state.email).then(response => {
                localStorage.setItem("reset_code", response.data)

                this.props.history.push('/resetPasswordCode/' + this.state.email);
            });
        }
    }

    changeEmailHandler= (event) => {
        this.setState({email: event.target.value});
    }

    render() {

        let validation = this.submitted ?this.validator.validate(this.state) : this.state.validation
        return (
            <div>
                <div className="ResetPassword">
                    <form>
                        <h4 align={'center'}>Reset your password: </h4>
                        <p align={'center'} style={{ fontStyle: 'italic', color: "#00FF00" }}>-A code will be send to your email-</p>

                        <div className={validation.email.isInvalid ? 'has-error' : undefined}>
                            <label>Email</label>
                            <input style={inputStyle} type="text" className="form-control" placeholder="Enter email" value={this.state.email} onChange={this.changeEmailHandler} required />
                            <span className="help-block" style={{color:'red'}}>{validation.email.message}</span>
                        </div>

                    </form>

                    <button type="submit" className="btn btn-primary btn-block" onClick={this.sendCode}>Send Code</button>

                </div>
            </div>
        );
    }
}
