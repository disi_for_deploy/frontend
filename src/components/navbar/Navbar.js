import React, { Component } from 'react'
import logo from '../../tennis-ball-svgrepo-com.svg';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Button
} from 'reactstrap';
import { Dropdown, NavDropdown } from "react-bootstrap";
import accLogo from '../../commons/images/acc.png';
import jwt from 'jwt-decode';
import ClientService from "../../services/ClientService";
import {withRouter} from "react-router";
import Cookies from 'universal-cookie';
const cookies = new Cookies();

class NavigationBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            role: '',
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            client: '',
            history: '',
            id: ''
        }

        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        this.getRole();
        this.getUsername();
    }

    getRole() {
        if (localStorage.getItem("login_token") === null) {
            this.setState({ role: '' }, function () {
                console.log("empty");
            });
        } else {
            const userRole = jwt(localStorage.getItem("login_token"));
            if (userRole.roles[0] === 'client') {
                this.setState({ role: "client" }, function () {

                });
            } else if (userRole.roles[0] === 'admin') {
                this.setState({ role: "admin" }, function () {

                });
            }
        }
    }

    getUsername() {
        if (localStorage.getItem("login_token") === null) {
            this.setState({ username: '' });
        } else {
            let user = jwt(localStorage.getItem("login_token")).sub;
            this.setState({ username: user }, function () {
                if (this.state.role === "client") {
                    this.getClient();
                }
            });

        }
    }

    getClient() {
        if (this.state.username !== '') {
            ClientService.getClient(this.state.username).then(response => {
                this.setState({ firstName: response.data.firstName });
                this.setState({ lastName: response.data.lastName });
                sessionStorage.setItem("client", JSON.stringify(response.data));
            });
        }
    }

    logout() {
        localStorage.removeItem("login_token");
        sessionStorage.removeItem('client');
        cookies.remove("Request_partner");

        this.props.history.push('/')
        window.location.reload(false);
    }

    render() {

        return (

            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">
                        <img src={logo} alt="" width={"50"} height={"35"} />
                        TennisClub
                    </NavbarBrand>
                    <NavbarToggler />
                    <Collapse navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="/">
                                    <i className="fa fa-home" /> Home
                                </NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/locations">
                                    <i className="fa fa-map-marker" /> Locations
                                </NavLink>
                            </NavItem>
                            {this.state.role === "client" &&
                            <NavItem>
                                <NavLink href="/notifications">
                                    <i className="fa fa-envelope" /> Notifications
                                </NavLink>
                            </NavItem>}
                        </Nav>
                    </Collapse>
                    <NavbarToggler />
                    {this.state.role === '' && <Button color="outline-secondary" className="justify-content-end" href="/login">Login</Button>}
                    {this.state.role === "client" && <div className="mb-2"><NavDropdown
                        id={`dropdown-button-drop`}
                        title={
                            <div className="pull-left">
                                <img className="thumbnail-image"
                                    src={accLogo}
                                    alt="acc pic"
                                    width={'35'}
                                />

                            </div>
                        }>
                        <Dropdown.Item active style={{ backgroundColor: 'white', color: 'black' }}><b>{this.state.firstName} {this.state.lastName}</b><br />{this.state.username}</Dropdown.Item>
                        <Dropdown.Divider />

                        <Dropdown.Item a={'true'} href={'/client/' + this.state.username}>Edit profile</Dropdown.Item>
                        <Dropdown.Item a={'true'} href={'/wallet/' + this.state.username}>My wallet</Dropdown.Item>
                        <Dropdown.Item a={'true'} href={'/myReservations/' + this.state.username}>View reservations</Dropdown.Item>
                        <Dropdown.Item a={'true'} href={'/mySubscriptions/' + this.state.username}>View subscriptions</Dropdown.Item>
                        <Dropdown.Item a={'true'} href={'/joinReservation/' + this.state.username}>Join reservation</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item onClick={this.logout}>Sign Out</Dropdown.Item>

                    </NavDropdown></div>}
                    {this.state.role === "admin" && <div className="mb-2"><NavDropdown
                        id={`dropdown-button-drop`}
                        title={
                            <div className="pull-left">
                                <img className="thumbnail-image"
                                    src={accLogo}
                                    alt="acc pic"
                                    width={'35'}
                                />

                            </div>
                        }>
                        <Dropdown.Item active style={{ backgroundColor: 'white', color: 'black' }}><b>{this.state.username}</b></Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item a={'true'} href={'/adminHomePage'}>Manage</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item onClick={this.logout}>Sign Out</Dropdown.Item>
                    </NavDropdown></div>}

                </Navbar>

            </div>

        )
    };

} export default withRouter(NavigationBar)