import React, { Component } from "react";
import './Locations.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import ManageLocationsService from "../../services/ManageLocationsService"

export default class Locations extends Component {

    constructor(props) {
        super(props)

        this.state = {
            locations: []
        }

        this.handleLocation = this.handleLocation.bind(this);
    }

    componentDidMount() {
        ManageLocationsService.getAllLocations().then(response => {
            this.setState({ locations: response.data })
        });
    }

    handleLocation = event => {
        ManageLocationsService.getLocationById(event.target.id).then(response => {
            this.setState({ location: response.data })
           // this.props.history.push({"/location/" + event.target.id, {state: {lat: propOneValue}}});
            this.props.history.push({
                pathname: "/location/" + event.target.id,
                state: {
                    lat: response.data.latitude,
                    lng: response.data.longitude
                }
            });
        });

       // this.props.history.push({"/location/"+ event.target.id, state: {lat: propOneValue}});
    }

    render() {

        return (
            <div className="container">
                <h1 align={'center'} style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799" }}>Locations</h1>
                <div className="row" id="ads">
                    {this.state.locations.map((location,i) =>
                        <div key={i} className="col-md-4" style={{marginTop:"3%"}}>
                            <div className="card rounded">
                                <div className="card-image">
                                    <img className="img-fluid" src={require('../../commons/images/locations/location' + (i + 5) + '.jpg')} style={{height:"300px"}} alt="Alternate Text" />
                                </div>
                                <div className="text-center">
                                    <div>
                                        <h5>{location.name}</h5>
                                    </div>
                                    <button style={{marginBottom:"2%"}} id={location.idLocation} type="button" className="btn btn-primary"  onClick={this.handleLocation}>View Courts</button>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
