import React, { Component } from "react";
import './Locations.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css'
import ManageCourtService from "../../services/ManageCourtsService"
import ManageLocationsService from "../../services/ManageLocationsService"
import TariffService from "../../services/TariffService"
import jwt from 'jwt-decode'
import swal from 'sweetalert'

export default class Courts extends Component {

    constructor(props) {
        super(props)

        this.state = {
            location: '',
            courts: [],
            defaultLocation: { lat: this.props.location.state.lat, lng: this.props.location.state.lng },
            zoom: 10,
            id: this.props.match.params.id,
            weekDayS: '',
            weekNightS: '',
            weekendDayS: '',
            weekendNightS: '',
            weekDayW: '',
            weekNightW: '',
            weekendDayW: '',
            weekendNightW: '',
        }

        this.handleSubscription = this.handleSubscription.bind(this);
        this.handleReservation = this.handleReservation.bind(this);
        this.handleDisponibility = this.handleDisponibility.bind(this);
    }

    componentDidMount() {
        ManageLocationsService.getLocationById(this.state.id).then(response => {
            this.setState({ location: response.data })
        });

        ManageCourtService.getAllCourts().then(response => {
            this.setState({ courts: response.data })
        })

        TariffService.getTariffsByLocationId(this.state.id).then(response => {

            for (var i = 0; i < 8; i++) {
                if (response.data[i].season === 'Summer' && response.data[i].dayOfWeek === 'Workweek' && response.data[i].night === true) {
                    this.setState({ weekNightS: response.data[i].price });
                }
                if (response.data[i].season === 'Summer' && response.data[i].dayOfWeek === 'Workweek' && response.data[i].night === false) {
                    this.setState({ weekDayS: response.data[i].price });
                }
                if (response.data[i].season === 'Summer' && response.data[i].dayOfWeek === 'Weekend' && response.data[i].night === true) {
                    this.setState({ weekendNightS: response.data[i].price });
                }
                if (response.data[i].season === 'Summer' && response.data[i].dayOfWeek === 'Weekend' && response.data[i].night === false) {
                    this.setState({ weekendDayS: response.data[i].price });
                }
                if (response.data[i].season === 'Winter' && response.data[i].dayOfWeek === 'Workweek' && response.data[i].night === true) {
                    this.setState({ weekNightW: response.data[i].price });
                }
                if (response.data[i].season === 'Winter' && response.data[i].dayOfWeek === 'Workweek' && response.data[i].night === false) {
                    this.setState({ weekDayW: response.data[i].price });
                }
                if (response.data[i].season === 'Winter' && response.data[i].dayOfWeek === 'Weekend' && response.data[i].night === true) {
                    this.setState({ weekendNightW: response.data[i].price });
                }
                if (response.data[i].season === 'Winter' && response.data[i].dayOfWeek === 'Weekend' && response.data[i].night === false) {
                    this.setState({ weekendDayW: response.data[i].price });
                }
            }
        })

        var map =new window.google.maps.Map(document.getElementById('map'), {
            center: this.state.defaultLocation,
            zoom: 14,
            mapTypeId: 'roadmap',
            defaultLocation: this.state.defaultLocation
        });

        var marker = new window.google.maps.Marker({
            position: this.state.defaultLocation,
            title: this.state.location.name
        });

        // To add the marker to the map, call setMap();
        marker.setMap(map);

    }

    handleSubscription = event => {
        const token =   localStorage.getItem("login_token");
        const userRole = jwt(token); // decode token here

        if (localStorage.getItem("login_token") === null) {
            this.props.history.push('/login')
        } else if(userRole.roles[0] === 'client'){
             this.props.history.push('/makeSubscription/' + event.target.id)
        }else{
            swal("Warning", "Only authenticated clients can access this.", "warning");
        }
    }

    handleReservation = event => {
        const token =   localStorage.getItem("login_token");
        const userRole = jwt(token); // decode token here

        if (localStorage.getItem("login_token") === null) {
            this.props.history.push('/login')
        } else if(userRole.roles[0] === 'client'){
             this.props.history.push('/makeReservation/' + event.target.id)
        }else{
            swal("Warning", "Only authenticated clients can access this.", "warning");
        }
    }

    handleDisponibility = event => {

        this.props.history.push('/courtDisponibility/' + event.target.id);
    }

    render() {

        return (
            <div>
                <div className="container">
                    <div className="row" style={{marginTop: "3%" }}>

                        <div className="column" style={{maxWidth: "15%" }}>
                            <div className="col-sm" style={{marginTop: "15%" }}>
                                <div id='map' style={{ height: '500px', width: "500px" }} />
                            </div>
                        </div>

                        <div className="column" style={{maxWidth: "80%", marginLeft: "5%", marginTop: "3%" }}>
                            <h1 style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799", marginLeft: "30%", textAlign: "center" }}>{this.state.location.name}</h1>
                            <h4 style={{ marginTop: "1%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799", marginLeft: "30%", textAlign: "center" }}>{this.state.location.address}</h4>
                            <h4 style={{ marginTop: "1%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799", marginLeft: "30%", textAlign: "center" }}>Program: {this.state.location.startHour} - {this.state.location.endHour}</h4>

                            <div className="column" style={{maxWidth: "25%", marginLeft: "40%", marginTop: "10%",  float:"left" }}>
                                <h3 style={{ color: "white" }}>Summer Tariff</h3>

                                <table className="table table-dark" style={{ width: "10%"}}>
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Day</th>
                                        <th scope="col">Night</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Week</th>
                                        <td>{this.state.weekDayS}</td>
                                        <td>{this.state.weekNightS}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Weekend</th>
                                        <td>{this.state.weekendDayS}</td>
                                        <td>{this.state.weekendNightS}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div className="column" style={{maxWidth: "25%", marginLeft:"10%", marginTop: "10%",  float:"left"}}>
                                <h3 style={{ color: "white" }}>Winter Tariff</h3>
                                <table className="table table-dark" style={{ width: "10%" }}>
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Day</th>
                                        <th scope="col">Night</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Week</th>
                                        <td>{this.state.weekDayW}</td>
                                        <td>{this.state.weekNightW}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Weekend</th>
                                        <td>{this.state.weekendDayW}</td>
                                        <td>{this.state.weekendNightW}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br/><br/><br/><br/>
                    <div className="row" id="ads">
                        <h1 style={{ marginTop: "3%", fontWeight: "bold", color: "white", textShadow: "2px 2px #5D6799", marginLeft: "45%", marginBottom: "2%" }}>Courts</h1>
                        {this.state.courts.filter(court => court.locationAddress === this.state.location.address).map((court, i) =>
                            <div key={i} className="col-md-4" style={{ marginTop: "1%" }}>
                                <div className="card rounded">
                                    <div className="card-image">
                                        <img className="img-fluid" src={require('../../commons/images/locations/location' + i + '.jpg')} style={{ height: "300px" }} alt="Alternate Text" />
                                    </div>
                                    <div className="text-center" style={{marginBottom:"2%"}}>
                                        <div>
                                            <h5>COURT {court.courtNumber} - {court.type}</h5>
                                            <h6>{court.details}</h6>
                                        </div>
                                        <button id={court.idCourt} style={{marginLeft: "5px", textDecoration: "none" }} onClick={this.handleDisponibility} className="card-detail-badge">Availability</button>
                                        <button id={court.idCourt} style={{textDecoration: "none" }} onClick={this.handleReservation} className="card-detail-badge">Reservation</button>
                                        <button id={court.idCourt} style={{textDecoration: "none" }} onClick={this.handleSubscription} className="card-detail-badge">Subscription</button>
                                    </div>
                                </div>
                                <br/>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}